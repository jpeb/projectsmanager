﻿using ProjectsManager.UI.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectsManager.UI
{
    internal class AppController : IDisposable
    {
        private readonly IProjectsService _projectsService;

        public AppController(IProjectsService projectsService)
        {
            _projectsService = projectsService;
        }

        public async Task Query1()
        {
            Console.Write("\nEnter user id: ");

            int userId;
            bool idValid;
            do
            {
                Console.Write("\n > ");
                idValid = int.TryParse(Console.ReadLine(), out userId);

                if (!idValid)
                    Utils.WriteColored("\nAn error occurred. Please try again.",ConsoleColor.DarkRed);
            } while (!idValid);

            var result = await _projectsService.GetUserProjectTasksNumber(userId);

            if(result.Count == 0)
            {
                Utils.WriteColored($"\n\nUser with id={userId} not found or doesn't have a project\n", ConsoleColor.Red);
                return;
            }

            var data = result.First();

            Console.Write("\n\nProject: ");
            Utils.WriteColored($"{data.Project.Name}\n", ConsoleColor.Green);
            Console.Write("Description: ");
            Utils.WriteColored($"{data.Project.Description}\n", ConsoleColor.Green);
            Console.Write("CreatedAt: ");
            Utils.WriteColored($"{data.Project.CreatedAt}\n", ConsoleColor.Green);
            Console.Write("Deadline: ");
            Utils.WriteColored($"{data.Project.Deadline}\n", ConsoleColor.Green);
            Console.Write("AuthorId: ");
            Utils.WriteColored($"{data.Project.AuthorId}\n", ConsoleColor.Green);
            Console.Write("TeamId: ");
            Utils.WriteColored($"{data.Project.TeamId}\n", ConsoleColor.Green);

            Console.Write("\n► Tasks number: ");
            Utils.WriteColored($"{data.TasksNumber}\n", ConsoleColor.DarkCyan);
        }

        public async Task Query2()
        {
            Console.Write("\nEnter user id: ");

            int userId;
            bool idValid;
            do
            {
                Console.Write("\n > ");
                idValid = int.TryParse(Console.ReadLine(), out userId);

                if (!idValid)
                    Utils.WriteColored("\nAn error occurred. Please try again.", ConsoleColor.DarkRed);
            } while (!idValid);

            Console.Write("\nEnter max task name length: ");

            int maxNameLength;
            do
            {
                Console.Write("\n > ");
                idValid = int.TryParse(Console.ReadLine(), out maxNameLength);

                if (!idValid)
                    Utils.WriteColored("\nAn error occurred. Please try again.", ConsoleColor.DarkRed);
            } while (!idValid);

            var result = await _projectsService.GetUserTasksWithNameLessThen(userId, maxNameLength);

            if (result.Count == 0)
            {
                Utils.WriteColored("\n\nNothing found:(\n", ConsoleColor.Red);
                return;
            }

            Console.WriteLine($"\n\n► User(ID = {userId}) tasks with name length less than {maxNameLength}:\n");
            int number = 1;
            foreach (var task in result)
            {
                Utils.WriteColored($" {number++}", ConsoleColor.Green);
                Console.Write(" | Name: ");
                Utils.WriteColored(task.Name, ConsoleColor.Green);
                Console.Write(" | State: ");
                Utils.WriteColored(task.State.ToString(), ConsoleColor.Green);
                Console.Write(" | CreatedAt: ");
                Utils.WriteColored(task.CreatedAt.ToString(), ConsoleColor.Green);
                Console.Write(" | FinishedAt: ");
                Utils.WriteColored(task?.FinishedAt?.ToString() ?? "Not finished", ConsoleColor.Green);
                Console.Write("\n Description: ");
                Utils.WriteColored($"{task.Description}\n\n", ConsoleColor.Green);
            }
        }

        public async Task Query3()
        {
            Console.Write("\nEnter user id: ");

            int userId;
            bool idValid;
            do
            {
                Console.Write("\n > ");
                idValid = int.TryParse(Console.ReadLine(), out userId);

                if (!idValid)
                    Utils.WriteColored("\nAn error occurred. Please try again.", ConsoleColor.DarkRed);
            } while (!idValid);

            Console.Write("\nEnter year: ");

            int year;
            do
            {
                Console.Write("\n > ");
                idValid = int.TryParse(Console.ReadLine(), out year);

                if (!idValid)
                    Utils.WriteColored("\nAn error occurred. Please try again.", ConsoleColor.DarkRed);
            } while (!idValid);

            var result = await _projectsService.GetUserTasksFinishedInYear(userId, year);

            if (result.Count == 0)
            {
                Utils.WriteColored("\n\nNothing found:(\n", ConsoleColor.Red);
                return;
            }

            Console.WriteLine($"\n\n► User(ID = {userId}) tasks finished in {year}:");
            int number = 1;
            foreach (var task in result)
            {
                Utils.WriteColored($"\n\n {number++}", ConsoleColor.Green);
                Console.Write(" | ID: ");
                Utils.WriteColored(task.Id.ToString(), ConsoleColor.Green);
                Console.Write(" | Name: ");
                Utils.WriteColored(task.Name, ConsoleColor.Green);
            }
        }

        public async Task Query4()
        {
            Console.Write("\nEnter min user age in years: ");

            int year;
            bool idValid;
            do
            {
                Console.Write("\n > ");
                idValid = int.TryParse(Console.ReadLine(), out year);

                if (!idValid)
                    Utils.WriteColored("\nAn error occurred. Please try again.", ConsoleColor.DarkRed);
            } while (!idValid);

            var result = await _projectsService.GetTeamsWithUsersOlderThan(year);

            if (result.Count == 0)
            {
                Utils.WriteColored("\n\nNothing found:(\n", ConsoleColor.Red);
                return;
            }

            Console.WriteLine($"\n\n► Teams and their users:");
            int number = 1;
            var currentDate = DateTime.Now;
            foreach (var team in result)
            {
                Utils.WriteColored($"\n\n Team {number++}", ConsoleColor.Green);
                Console.Write(" | ID: ");
                Utils.WriteColored(team.Id.ToString(), ConsoleColor.Green);
                Console.Write(" | Name: ");
                Utils.WriteColored($"{team.Name}\n", ConsoleColor.Green);
                
                foreach (var user in team.Users)
                    Console.WriteLine($"  {user.FirstName} {user.LastName} | {user.Email} | BirthDay: {user.BirthDay.ToShortDateString()} | Age: {(currentDate.Year - user.BirthDay.Year)}");
                
            }
        }

        public async Task Query5()
        {

            var result = await _projectsService.GetUsersWithTasks();

            if (result.Count == 0)
            {
                Utils.WriteColored("\n\nNothing found:(\n", ConsoleColor.Red);
                return;
            }

            Console.WriteLine($"\n\n► Users and their tasks:");
            foreach (var userTasks in result)
            {
                Utils.WriteColored($"\n\n► User {userTasks.Id}", ConsoleColor.Blue);
                Console.Write(" | ");
                Utils.WriteColored($"{userTasks.FirstName} {userTasks.LastName}", ConsoleColor.Green);
                Console.Write(" | ");
                Utils.WriteColored(userTasks.Email, ConsoleColor.Green);
                Console.Write(" | BirthDay: ");
                Utils.WriteColored(userTasks.BirthDay.ToShortDateString(), ConsoleColor.Green);
                Console.Write(" | RegisteredAt: ");
                Utils.WriteColored($"{userTasks.RegisteredAt.ToShortDateString()}\n\n", ConsoleColor.Green);
                Utils.WriteColored("  TASKS:\n\n", ConsoleColor.Cyan);

                foreach (var task in userTasks.Tasks)
                {
                    Console.Write("  ID: ");
                    Utils.WriteColored(task.Id.ToString(), ConsoleColor.Green);
                    Console.Write(" | Name: ");
                    Utils.WriteColored(task.Name, ConsoleColor.Green);
                    Console.Write(" | State: ");
                    Utils.WriteColored(task.State.ToString(), ConsoleColor.Green);
                    Console.Write(" | CreatedAt: ");
                    Utils.WriteColored(task.CreatedAt.ToShortDateString(), ConsoleColor.Green);
                    Console.Write(" | FinishedAt: ");
                    Utils.WriteColored(task?.FinishedAt?.ToShortDateString() ?? "Not finished", ConsoleColor.Green);
                    Console.Write("\n  Description: ");
                    Utils.WriteColored($"{task.Description}\n\n", ConsoleColor.Green);
                }
            }
        }

        public async Task Query6()
        {
            Console.Write("\nEnter user id: ");

            int userId;
            bool idValid;
            do
            {
                Console.Write("\n > ");
                idValid = int.TryParse(Console.ReadLine(), out userId);

                if (!idValid)
                    Utils.WriteColored("\nAn error occurred. Please try again.", ConsoleColor.DarkRed);
            } while (!idValid);

            var result = await _projectsService.GetUserTaskStatictics(userId);

            if (result is null)
            {
                Utils.WriteColored($"\n\nUser with id={userId} not found\n", ConsoleColor.Red);
                return;
            }

            Utils.WriteColored(" User:\n", ConsoleColor.Cyan);
            Console.Write(" ID: ");
            Utils.WriteColored($"{result.User.Id}", ConsoleColor.Green);
            Console.Write(" | Name: ");
            Utils.WriteColored($" {result.User.FirstName} {result.User.LastName}", ConsoleColor.Green);
            Console.Write(" | Email: ");
            Utils.WriteColored($" {result.User.Email}\n", ConsoleColor.Green);

            if (result.LastUserProject is null)
            {
                Utils.WriteColored($"  User doesn't have project", ConsoleColor.Red);
                return;
            }

            Utils.WriteColored("\n Last user project:\n", ConsoleColor.Cyan);
            Console.Write(" ID: ");
            Utils.WriteColored($"{result.LastUserProject.Id}", ConsoleColor.Green);
            Console.Write(" | Name: ");
            Utils.WriteColored($"{result.LastUserProject.Name}", ConsoleColor.Green);
            Console.Write(" | CreatedAt: ");
            Utils.WriteColored($"{result.LastUserProject.CreatedAt}", ConsoleColor.Green);
            Console.Write(" | Deadline: ");
            Utils.WriteColored($"{result.LastUserProject.Deadline}\n", ConsoleColor.Green);
            Console.Write(" Description: ");
            Utils.WriteColored($"{result.LastUserProject.Description}\n", ConsoleColor.Green);

            Utils.WriteColored("\n Last users project tasks number: ", ConsoleColor.Cyan);
            Utils.WriteColored($"{result.AllUsersLastProjectTasksNumber}\n", ConsoleColor.Green);

            Utils.WriteColored("\n All user unfinished tasks number: ", ConsoleColor.Cyan);
            Utils.WriteColored($"{result.UnfinishedUserTasksNumber}\n", ConsoleColor.Green);

            Utils.WriteColored("\n User longest task:\n", ConsoleColor.Cyan);
            if(result.LongestUserTask is null)
            {
                Utils.WriteColored($"  User doesn't have tasks", ConsoleColor.Red);
                return;
            }
            Console.Write(" ID: ");
            Utils.WriteColored($"{result.LongestUserTask.Id}", ConsoleColor.Green);
            Console.Write(" | Name: ");
            Utils.WriteColored(result.LongestUserTask.Name, ConsoleColor.Green);
            Console.Write(" | State: ");
            Utils.WriteColored(result.LongestUserTask.State.ToString(), ConsoleColor.Green);
            Console.Write(" | CreatedAt: ");
            Utils.WriteColored(result.LongestUserTask.CreatedAt.ToString(), ConsoleColor.Green);
            Console.Write(" | FinishedAt: ");
            Utils.WriteColored(result.LongestUserTask?.FinishedAt?.ToString() ?? "Not finished", ConsoleColor.Green);
            Console.Write("\n Description: ");
            Utils.WriteColored($"{result.LongestUserTask.Description}\n\n", ConsoleColor.Green);
        }

        public async Task Query7()
        {
            var result = await _projectsService.GetProjectStatictics();

            if (result.Count == 0)
            {
                Utils.WriteColored("\n\nNothing found:(\n", ConsoleColor.Red);
                return;
            }

            Console.WriteLine($"\n\n► Projects info:");
            foreach (var projectInfo in result)
            {
                Utils.WriteColored($"\n\n► Project {projectInfo.Project.Id}", ConsoleColor.Blue);
                Console.Write(" | ");
                Utils.WriteColored($"{projectInfo.Project.Name}", ConsoleColor.Green);
                Console.Write(" | AuthorId: ");
                Utils.WriteColored($"{projectInfo.Project.AuthorId}", ConsoleColor.Green);
                Console.Write(" | CreatedAt: ");
                Utils.WriteColored(projectInfo.Project.CreatedAt.ToShortDateString(), ConsoleColor.Green);
                Console.Write(" | Deadline: ");
                Utils.WriteColored(projectInfo.Project.Deadline.ToShortDateString(), ConsoleColor.Green);
                Console.Write("\n  TeamId: ");
                Utils.WriteColored($"{projectInfo.Project.TeamId}", ConsoleColor.Green);
                Console.Write(" | Description: ");
                Utils.WriteColored($"{projectInfo.Project.Description}\n\n", ConsoleColor.Green);

                if (projectInfo.LongestTaskByDescription is null)
                {
                    Utils.WriteColored($"  Project doesn't have tasks", ConsoleColor.Red);
                    continue;
                }

                Utils.WriteColored("\n Longest task by description: ", ConsoleColor.Cyan);
                Console.Write("\n  ID: ");
                Utils.WriteColored(projectInfo.LongestTaskByDescription.Id.ToString(), ConsoleColor.Green);
                Console.Write(" | Name: ");
                Utils.WriteColored(projectInfo.LongestTaskByDescription.Name, ConsoleColor.Green);
                Console.Write(" | State: ");
                Utils.WriteColored(projectInfo.LongestTaskByDescription.State.ToString(), ConsoleColor.Green);
                Console.Write(" | CreatedAt: ");
                Utils.WriteColored(projectInfo.LongestTaskByDescription.CreatedAt.ToShortDateString(), ConsoleColor.Green);
                Console.Write(" | FinishedAt: ");
                Utils.WriteColored(projectInfo.LongestTaskByDescription?.FinishedAt?.ToShortDateString() ?? "Not finished", ConsoleColor.Green);
                Console.Write("\n  Description: ");
                Utils.WriteColored($"{projectInfo.LongestTaskByDescription.Description}\n", ConsoleColor.Green);

                Utils.WriteColored("\n Shortest task by name: ", ConsoleColor.Cyan);
                Console.Write("\n  ID: ");
                Utils.WriteColored(projectInfo.ShortestTaskByName.Id.ToString(), ConsoleColor.Green);
                Console.Write(" | Name: ");
                Utils.WriteColored(projectInfo.ShortestTaskByName.Name, ConsoleColor.Green);
                Console.Write(" | State: ");
                Utils.WriteColored(projectInfo.ShortestTaskByName.State.ToString(), ConsoleColor.Green);
                Console.Write(" | CreatedAt: ");
                Utils.WriteColored(projectInfo.ShortestTaskByName.CreatedAt.ToShortDateString(), ConsoleColor.Green);
                Console.Write(" | FinishedAt: ");
                Utils.WriteColored(projectInfo.ShortestTaskByName?.FinishedAt?.ToShortDateString() ?? "Not finished", ConsoleColor.Green);
                Console.Write("\n  Description: ");
                Utils.WriteColored($"{projectInfo.ShortestTaskByName.Description}\n", ConsoleColor.Green);

                Utils.WriteColored("\n Number of users in team: ", ConsoleColor.Cyan);
                Utils.WriteColored($"{(projectInfo.UsersNumber is null ? "Not calculated by condition" : projectInfo.UsersNumber)}\n", ConsoleColor.Green);
            }
        }

        public async Task StartDeferredTask()
        {
            Utils.WriteColored($"\n Task has been successfully started\n", ConsoleColor.Cyan);

            var markedTaskId = await _projectsService.MarkRandomTaskWithDelay(1000);

            Utils.WriteColored($"\n\n Task with ID={markedTaskId} marked as finished\n\n", ConsoleColor.Yellow);
        }

        public void Dispose()
        {
            _projectsService.Dispose();
        }
    }
}
