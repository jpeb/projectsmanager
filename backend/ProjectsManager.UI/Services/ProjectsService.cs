﻿using ProjectsManager.Common.DTOs.Project;
using ProjectsManager.Common.DTOs.Task;
using ProjectsManager.Common.DTOs.Team;
using ProjectsManager.Common.DTOs.User;
using ProjectsManager.Common.Enums;
using ProjectsManager.UI.Configuration;
using ProjectsManager.UI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using System.Timers;

namespace ProjectsManager.UI.Services
{
    public class ProjectsService : IDisposable, IProjectsService
    {
        private static Random random = new Random();
        private readonly HttpClient _httpClient;

        public ProjectsService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        // Query 1
        public Task<ICollection<ProjectTasksNumberDTO>> GetUserProjectTasksNumber(int userId)
        {
            return _httpClient.GetFromJsonAsync<ICollection<ProjectTasksNumberDTO>>(
                    ApiUris.UserProjectsTasksNumber(userId));
        }

        // Query 2
        public Task<ICollection<TaskDTO>> GetUserTasksWithNameLessThen(int userId, int maxTaskNameLength)
        {
            return _httpClient.GetFromJsonAsync<ICollection<TaskDTO>>(
                    ApiUris.UserTasksWithNameLessThen(userId, maxTaskNameLength));
        }

        // Query 3
        public Task<ICollection<TaskNameDTO>> GetUserTasksFinishedInYear(int userId, int finishedYear)
        {
            return _httpClient.GetFromJsonAsync<ICollection<TaskNameDTO>>(
                    ApiUris.UserTasksFinishedInYear(finishedYear, userId));
        }

        // Query 4
        public Task<ICollection<TeamUsersDTO>> GetTeamsWithUsersOlderThan(int minYearsNumber)
        {
            return _httpClient.GetFromJsonAsync<ICollection<TeamUsersDTO>>(
                    ApiUris.TeamUsersOlderThan(minYearsNumber));
        }


        // Query 5
        public Task<ICollection<UserTasksDTO>> GetUsersWithTasks()
        {
            return _httpClient.GetFromJsonAsync<ICollection<UserTasksDTO>>(
                    ApiUris.UsersWithTasks());
        }


        // Query 6
        public Task<UserTaskStaticticsDTO> GetUserTaskStatictics(int userId)
        {
            return _httpClient.GetFromJsonAsync<UserTaskStaticticsDTO>(
                    ApiUris.UserTaskStatictics(userId));
        }


        // Query 7
        public Task<ICollection<ProjectStaticticsDTO>> GetProjectStatictics()
        {
            return _httpClient.GetFromJsonAsync<ICollection<ProjectStaticticsDTO>>(
                    ApiUris.ProjectStatictics());
        }

        // Mark random task with delay
        public Task<int> MarkRandomTaskWithDelay(int delay)
        {
            var tsc = new TaskCompletionSource<int>();
            var timer = new Timer(delay)
            {
                AutoReset = false
            };

            timer.Elapsed += async (s, e) =>
            {
                var tasks = await _httpClient.GetFromJsonAsync<ICollection<TaskDTO>>(ApiUris.Tasks);

                var randomTask = tasks.ElementAt(random.Next(tasks.Count));

                var updatedTask = new UpdateTaskDTO
                {
                    Description = randomTask.Description,
                    Name = randomTask.Name,
                    PerformerId = randomTask.PerformerId,
                    ProjectId = randomTask.ProjectId,
                    State = (int)TaskState.Finished,
                    FinishedAt = DateTime.Now
                };

                var result = await _httpClient.PutAsJsonAsync($"{ApiUris.Tasks}/{randomTask.Id}", updatedTask);

                if (result.IsSuccessStatusCode)
                    tsc.SetResult(randomTask.Id);
                else
                    tsc.SetException(new Exception("Error while updating task"));

            };

            timer.Start();

            return tsc.Task;
        }

        public void Dispose()
        {
            _httpClient.Dispose();
        }
    }
}
