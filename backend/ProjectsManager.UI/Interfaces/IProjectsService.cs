﻿using ProjectsManager.Common.DTOs.Project;
using ProjectsManager.Common.DTOs.Task;
using ProjectsManager.Common.DTOs.Team;
using ProjectsManager.Common.DTOs.User;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectsManager.UI.Interfaces
{
    public interface IProjectsService : IDisposable
    {
        // Query 1
        Task<ICollection<ProjectTasksNumberDTO>> GetUserProjectTasksNumber(int userId);

        // Query 2
        Task<ICollection<TaskDTO>> GetUserTasksWithNameLessThen(int userId, int maxTaskNameLength);

        // Query 3
        Task<ICollection<TaskNameDTO>> GetUserTasksFinishedInYear(int userId, int finishedYear);

        // Query 4
        Task<ICollection<TeamUsersDTO>> GetTeamsWithUsersOlderThan(int minYearsNumber);

        // Query 5
        Task<ICollection<UserTasksDTO>> GetUsersWithTasks();

        // Query 6
        Task<UserTaskStaticticsDTO> GetUserTaskStatictics(int userId);

        // Query 7
        Task<ICollection<ProjectStaticticsDTO>> GetProjectStatictics();

        public Task<int> MarkRandomTaskWithDelay(int delay);
    }
}
