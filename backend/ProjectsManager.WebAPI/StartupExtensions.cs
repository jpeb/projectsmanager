﻿using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ProjectsManager.BLL.MappingProfiles;
using ProjectsManager.BLL.Services;
using ProjectsManager.BLL.Services.Abstract;
using ProjectsManager.DAL.Context;
using ProjectsManager.DAL.Repositories;
using ProjectsManager.DAL.Repositories.Abstract;
using System.Linq;
using System.Reflection;

namespace ProjectsManager.WebAPI
{
    public static class StartupExtensions
    {
        public static void RegisterRepositories(this IServiceCollection services)
        {
            services.AddScoped<IUnitOfWork, EFUnitOfWork>();
        }

        public static void RegisterServices(this IServiceCollection services)
        {
            services.AddScoped<IProjectsService, ProjectsService>();
            services.AddScoped<ITasksService, TasksService>();
            services.AddScoped<IUsersService, UsersService>();
            services.AddScoped<ITeamsService, TeamsService>();
        }

        public static void RegisterAutoMapper(this IServiceCollection services)
        {
            services.AddAutoMapper(cfg =>
            {
                cfg.AddProfile<ProjectProfile>();
                cfg.AddProfile<TaskProfile>();
                cfg.AddProfile<UserProfile>();
                cfg.AddProfile<TeamProfile>();
            },
            Assembly.GetExecutingAssembly());
        }

        public static IApplicationBuilder MigrateDB(this IApplicationBuilder builder)
        {
            using (var serviceScope = builder.ApplicationServices.CreateScope())
            using (var context = serviceScope.ServiceProvider.GetRequiredService<ProjectsDbContext>())
            {
                context.Database.Migrate();
            }

            return builder;
        }

        public static IApplicationBuilder SeedTestData(this IApplicationBuilder builder, int teamsNumber, int usersNumber, int projectsNumber, int tasksNumber)
        {
            using (var serviceScope = builder.ApplicationServices.CreateScope())
            using (var context = serviceScope.ServiceProvider.GetRequiredService<ProjectsDbContext>())
            {
                if (!context.Users.Any())
                    context.SeedTestData(teamsNumber, usersNumber, projectsNumber, tasksNumber);
            }

            return builder;
        }
    }
}
