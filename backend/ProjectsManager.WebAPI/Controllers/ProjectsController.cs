﻿using Microsoft.AspNetCore.Mvc;
using ProjectsManager.BLL.Services.Abstract;
using ProjectsManager.Common.DTOs.Project;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectsManager.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectsService _projectsService;

        public ProjectsController(IProjectsService projectsService)
        {
            _projectsService = projectsService;
        }

        [HttpGet]
        public async Task<ActionResult<ICollection<ProjectDTO>>> GetAll()
        {
            return Ok(await _projectsService.GetAll());
        }

        [HttpGet("names")]
        public async Task<ActionResult<ICollection<ProjectNameDTO>>> GetAllNames()
        {
            return Ok(await _projectsService.GetAllNames());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ProjectDTO>> Get(int id)
        {
            return await _projectsService.Get(id);
        }

        [HttpGet("tasksNumber/{userId}")]
        public async Task<ActionResult<ICollection<ProjectTasksNumberDTO>>> GetUserProjectsTasksNumber(int userId)
        {
            return Ok(await _projectsService.GetUserProjectsTasksNumber(userId));
        }

        [HttpGet("statictics")]
        public async Task<ActionResult<ICollection<ProjectStaticticsDTO>>> GetProjectStatictics()
        {
            return Ok(await _projectsService.GetProjectStatictics(20, 3));
        }
        

       [HttpPost]
        public async Task<ActionResult> Add([FromBody] NewProjectDTO newProject)
        {
            var createdProject = await _projectsService.Add(newProject);

            return Created($"projects/{createdProject.Id}", createdProject);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Update(int id, [FromBody] UpdateProjectDTO newProject)
        {
            newProject.Id = id;
            await _projectsService.Update(newProject);

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await _projectsService.Delete(id);

            return NoContent();
        }
    }
}
