﻿using System;

namespace ProjectsManager.Common.DTOs.Team
{
    public class TeamDTO
    {
        public int Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public string Name { get; set; }
    }
}
