﻿using ProjectsManager.Common.DTOs.User;
using System.Collections.Generic;

namespace ProjectsManager.Common.DTOs.Team
{
    public class TeamUsersDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<UserDTO> Users { get; set; }
    }
}
