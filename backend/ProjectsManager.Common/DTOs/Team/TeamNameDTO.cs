﻿namespace ProjectsManager.Common.DTOs.Team
{
    public class TeamNameDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
