﻿using System.Text.Json.Serialization;

namespace ProjectsManager.Common.DTOs.Team
{
    public class UpdateTeamDTO
    {
        [JsonIgnore]
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
