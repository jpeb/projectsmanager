﻿using ProjectsManager.Common.DTOs.Team;
using System;

namespace ProjectsManager.Common.DTOs.User
{
    public class UserDTO
    {
        public int Id { get; set; }
        public int? TeamId { get; set; }
        public TeamNameDTO Team { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime RegisteredAt { get; set; }
        public DateTime BirthDay { get; set; }
    }
}
