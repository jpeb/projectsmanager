﻿using ProjectsManager.Common.DTOs.Project;
using ProjectsManager.Common.DTOs.Task;

namespace ProjectsManager.Common.DTOs.User
{
    public class UserTaskStaticticsDTO
    {
        public UserDTO User { get; set; }
        public ProjectDTO LastUserProject { get; set; }
        public int AllUsersLastProjectTasksNumber { get; set; }
        public int UnfinishedUserTasksNumber { get; set; }
        public TaskDTO LongestUserTask { get; set; }
    }
}
