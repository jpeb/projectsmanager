﻿using ProjectsManager.Common.DTOs.Task;
using System;
using System.Collections.Generic;

namespace ProjectsManager.Common.DTOs.User
{
    public class UserTasksDTO
    {
        public int Id { get; set; }
        public int? TeamId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime RegisteredAt { get; set; }
        public DateTime BirthDay { get; set; }
        public List<TaskDTO> Tasks { get; set; }
    }
}
