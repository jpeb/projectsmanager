﻿using ProjectsManager.Common.DTOs.Team;
using ProjectsManager.Common.DTOs.User;
using System;

namespace ProjectsManager.Common.DTOs.Project
{
    public class ProjectDTO
    {
        public int Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public int AuthorId { get; set; }
        public UserNameDTO Author { get; set; }
        public int TeamId { get; set; }
        public TeamNameDTO Team { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
    }
}
