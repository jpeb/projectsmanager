﻿using ProjectsManager.Common.DTOs.Task;

namespace ProjectsManager.Common.DTOs.Project
{
    public class ProjectStaticticsDTO
    {
        public ProjectDTO Project { get; set; }
        public TaskDTO LongestTaskByDescription { get; set; }
        public TaskDTO ShortestTaskByName { get; set; }
        public int? UsersNumber { get; set; }
    }
}
