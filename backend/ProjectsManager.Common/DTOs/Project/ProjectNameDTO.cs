﻿using ProjectsManager.Common.DTOs.Team;
using ProjectsManager.Common.DTOs.User;
using System;

namespace ProjectsManager.Common.DTOs.Project
{
    public class ProjectNameDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
