﻿using System;
using System.Text.Json.Serialization;

namespace ProjectsManager.Common.DTOs.Task
{
    public class UpdateTaskDTO
    {
        [JsonIgnore]
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int State { get; set; }
        public DateTime? FinishedAt { get; set; }
    }
}
