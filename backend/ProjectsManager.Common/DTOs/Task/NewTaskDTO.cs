﻿using System;

namespace ProjectsManager.Common.DTOs.Task
{
    public class NewTaskDTO
    {
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int State { get; set; }
        public DateTime? FinishedAt { get; set; }
    }
}
