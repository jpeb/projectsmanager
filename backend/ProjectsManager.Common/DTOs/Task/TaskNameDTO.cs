﻿namespace ProjectsManager.Common.DTOs.Task
{
    public class TaskNameDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
