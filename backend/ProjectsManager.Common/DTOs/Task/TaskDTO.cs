﻿using ProjectsManager.Common.DTOs.Project;
using ProjectsManager.Common.DTOs.User;
using System;

namespace ProjectsManager.Common.DTOs.Task
{
    public class TaskDTO
    {
        public int Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public int ProjectId { get; set; }
        public ProjectNameDTO Project { get; set; }
        public int PerformerId { get; set; }
        public UserNameDTO Performer { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int State { get; set; }
        public DateTime? FinishedAt { get; set; }
    }
}
