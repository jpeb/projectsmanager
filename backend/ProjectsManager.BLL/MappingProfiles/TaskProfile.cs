﻿using AutoMapper;
using ProjectsManager.Common.DTOs.Task;
using ProjectsManager.DAL.Entities;

namespace ProjectsManager.BLL.MappingProfiles
{
    public sealed class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<Task, TaskDTO>();
            CreateMap<Task, TaskNameDTO>();

            CreateMap<NewTaskDTO, Task>();
            CreateMap<UpdateTaskDTO, Task>();
        }
    }
}
