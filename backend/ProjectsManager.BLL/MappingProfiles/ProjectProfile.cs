﻿using AutoMapper;
using ProjectsManager.Common.DTOs.Project;
using ProjectsManager.DAL.Entities;

namespace ProjectsManager.BLL.MappingProfiles
{
    public sealed class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<Project, ProjectDTO>();
            CreateMap<Project, ProjectNameDTO>();

            CreateMap<NewProjectDTO, Project>();
            CreateMap<UpdateProjectDTO, Project>();
        }
    }
}
