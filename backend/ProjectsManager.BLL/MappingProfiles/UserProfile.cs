﻿using AutoMapper;
using ProjectsManager.Common.DTOs.User;
using ProjectsManager.DAL.Entities;
using System.Linq;

namespace ProjectsManager.BLL.MappingProfiles
{
    public sealed class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserDTO>()
                .ForMember(x => x.RegisteredAt, m => m.MapFrom(p => p.CreatedAt));

            CreateMap<User, UserNameDTO>()
                .ForMember(x => x.Name, m => m.MapFrom(p => $"{p.FirstName} {p.LastName}"));

            CreateMap<User, UserTasksDTO>()
                .ForMember(x => x.RegisteredAt, m => m.MapFrom(p => p.CreatedAt))
                .ForMember(x => x.Tasks, m => m.MapFrom(u => u.Tasks.OrderByDescending(t => t.Name.Length)));

            CreateMap<NewUserDTO, User>();
            CreateMap<UpdateUserDTO, User>();
        }
    }
}
