﻿using AutoMapper;
using ProjectsManager.Common.DTOs.Team;
using ProjectsManager.DAL.Entities;
using System.Linq;

namespace ProjectsManager.BLL.MappingProfiles
{
    public sealed class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<Team, TeamDTO>();
            CreateMap<Team, TeamUsersDTO>()
                .ForMember(t => t.Users, p => p.MapFrom(t => t.Users.OrderByDescending(u => u.CreatedAt)));
            CreateMap<Team, TeamNameDTO>();

            CreateMap<NewTeamDTO, Team>();
            CreateMap<UpdateTeamDTO, Team>();
        }
    }
}
