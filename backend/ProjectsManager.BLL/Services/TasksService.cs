﻿using AutoMapper;
using ProjectsManager.Common.DTOs.Task;
using ProjectsManager.BLL.Services.Abstract;
using ProjectsManager.DAL.Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TaskEntity = ProjectsManager.DAL.Entities.Task;
using ProjectsManager.BLL.Exceptions;
using Task = System.Threading.Tasks.Task;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace ProjectsManager.BLL.Services
{
    public class TasksService : BaseService, ITasksService
    {
        private readonly IUnitOfWork _context;
        private readonly IRepository<TaskEntity> _taskRepository;

        public TasksService(IMapper mapper, IUnitOfWork context) : base(mapper)
        {
            _context = context;
            _taskRepository = _context.TaskRepository;
        }

        public async Task<IEnumerable<TaskDTO>> GetAll()
        {
            var tasks = await _taskRepository.GetAllAsync(null, true,
                i => i.Include(p => p.Performer).Include(p => p.Project));

            return _mapper.Map<IEnumerable<TaskDTO>>(tasks);
        }

        public async Task<IEnumerable<TaskNameDTO>> GetAllNames()
        {
            var tasks = await _taskRepository.GetAllAsync();

            return _mapper.Map<IEnumerable<TaskNameDTO>>(tasks);
        }

        public async Task<TaskDTO> Get(int taskId)
        {
            var task = (await _taskRepository.GetAllAsync(p => p.Id == taskId, true,
                i => i.Include(p => p.Performer).Include(p => p.Project))).FirstOrDefault();

            if (task is null)
            {
                throw new NotFoundException(nameof(task), taskId);
            }

            return _mapper.Map<TaskDTO>(task);
        }

        public async Task<TaskDTO> Add(NewTaskDTO task)
        {
            var newTask = _taskRepository.Add(_mapper.Map<TaskEntity>(task));
            await _context.SaveChangesAsync();

            return _mapper.Map<TaskDTO>(newTask);
        }

        public async Task Delete(int taskId)
        {
            var task = await _taskRepository.GetByIdAsync(taskId);

            if (task is null)
            {
                throw new NotFoundException(nameof(task), taskId);
            }

            _taskRepository.Delete(task);
            await _context.SaveChangesAsync();
        }

        public async Task Update(UpdateTaskDTO newTask)
        {
            var task = await _taskRepository.GetByIdAsync(newTask.Id);

            if (task is null)
            {
                throw new NotFoundException(nameof(task), newTask.Id);
            }

            _mapper.Map(newTask, task);
            task.UpdatedAt = DateTime.Now;

            _taskRepository.Update(task);
            await _context.SaveChangesAsync();
        }

        // Query 2
        public async Task<ICollection<TaskDTO>> GetUserTasksWithNameLessThen(int userId, int maxTaskNameLength)
        {
            var tasks = await _taskRepository
                    .GetAllAsync(t => t.PerformerId == userId && t.Name.Length < maxTaskNameLength);

            return _mapper.Map<ICollection<TaskDTO>>(tasks);
        }

        // Query 3
        public async Task<ICollection<TaskNameDTO>> GetUserTasksFinishedInYear(int userId, int finishedYear)
        {
            var tasks = await _taskRepository
                .GetAllAsync(t => t.PerformerId == userId &&
                                t.FinishedAt.HasValue &&
                                t.FinishedAt.Value.Year == finishedYear);

            return _mapper.Map<ICollection<TaskNameDTO>>(tasks);
        }
    }
}
