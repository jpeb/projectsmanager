﻿using AutoMapper;
using ProjectsManager.BLL.Services.Abstract;
using ProjectsManager.DAL.Entities;
using ProjectsManager.DAL.Repositories.Abstract;
using System;
using System.Collections.Generic;
using ProjectsManager.Common.DTOs.Team;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ProjectsManager.BLL.Exceptions;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace ProjectsManager.BLL.Services
{
    public class TeamsService : BaseService, ITeamsService
    {
        private readonly IUnitOfWork _context;
        private readonly IRepository<Team> _teamRepository;

        public TeamsService(IMapper mapper, IUnitOfWork context) : base(mapper)
        {
            _context = context;
            _teamRepository = _context.TeamRepository;
        }

        public async Task<IEnumerable<TeamDTO>> GetAll()
        {
            var teams = await _teamRepository.GetAllAsync();

            return _mapper.Map<IEnumerable<TeamDTO>>(teams);
        }

        public async Task<IEnumerable<TeamNameDTO>> GetAllNames()
        {
            var teams = await _teamRepository.GetAllAsync();

            return _mapper.Map<IEnumerable<TeamNameDTO>>(teams);
        }

        public async Task<TeamDTO> Get(int teamId)
        {
            var team = await _teamRepository.GetByIdAsync(teamId);

            if (team is null)
            {
                throw new NotFoundException(nameof(team), teamId);
            }

            return _mapper.Map<TeamDTO>(team);
        }

        public async Task<TeamDTO> Add(NewTeamDTO team)
        {
            var newTeam = _teamRepository.Add(_mapper.Map<Team>(team));
            await _context.SaveChangesAsync();

            return _mapper.Map<TeamDTO>(newTeam);
        }

        public async Task Delete(int teamId)
        {
            var team = await _teamRepository.GetByIdAsync(teamId);

            if (team is null)
            {
                throw new NotFoundException(nameof(team), teamId);
            }

            _teamRepository.Delete(team);
            await _context.SaveChangesAsync();
        }

        public async Task Update(UpdateTeamDTO newTeam)
        {
            var team = await _teamRepository.GetByIdAsync(newTeam.Id);

            if (team is null)
            {
                throw new NotFoundException(nameof(team), newTeam.Id);
            }

            _mapper.Map(newTeam, team);
            team.UpdatedAt = DateTime.Now;

            _teamRepository.Update(team);
            await _context.SaveChangesAsync();
        }

        // Query 4
        public async Task<ICollection<TeamUsersDTO>> GetTeamUsersOlderThan(int minYearsNumber)
        {
            var currentYear = DateTime.Now.Year;

            return _mapper.Map<ICollection<TeamUsersDTO>>(
                await _teamRepository.GetAllAsync(
                    t => t.Users.Any() && !t.Users.Any(u => currentYear - u.BirthDay.Year < minYearsNumber),
                    true, s => s.Include(p => p.Users))
                );
        }
    }
}
