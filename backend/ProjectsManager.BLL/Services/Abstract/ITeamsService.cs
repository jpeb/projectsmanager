﻿using ProjectsManager.Common.DTOs.Team;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectsManager.BLL.Services.Abstract
{
    public interface ITeamsService
    {
        Task<IEnumerable<TeamDTO>> GetAll();
        Task<IEnumerable<TeamNameDTO>> GetAllNames();
        Task<TeamDTO> Get(int teamId);
        Task Update(UpdateTeamDTO team);
        Task<TeamDTO> Add(NewTeamDTO team);
        Task Delete(int teamId);
        
        // Query 4
        Task<ICollection<TeamUsersDTO>> GetTeamUsersOlderThan(int minYearsNumber);
    }
}
