﻿using ProjectsManager.Common.DTOs.User;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectsManager.BLL.Services.Abstract
{
    public interface IUsersService
    {
        Task<IEnumerable<UserDTO>> GetAll();
        Task<IEnumerable<UserNameDTO>> GetAllNames();
        Task<UserDTO> Get(int userId);
        Task Update(UpdateUserDTO user);
        Task<UserDTO> Add(NewUserDTO user);
        Task Delete(int userId);

        // Query 5
        Task<ICollection<UserTasksDTO>> GetUsersWithTasks();

        // Query 6
        Task<UserTaskStaticticsDTO> GetUserTaskStatictics(int userId);
    }
}
