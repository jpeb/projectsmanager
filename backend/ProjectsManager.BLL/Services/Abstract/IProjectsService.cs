﻿using ProjectsManager.Common.DTOs.Project;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectsManager.BLL.Services.Abstract
{
    public interface IProjectsService
    {
        Task<IEnumerable<ProjectDTO>> GetAll();
        Task<IEnumerable<ProjectNameDTO>> GetAllNames();

        Task<ProjectDTO> Get(int projectId);
        Task Update(UpdateProjectDTO project);
        Task<ProjectDTO> Add(NewProjectDTO project);
        Task Delete(int projectId);

        // Query 1
        Task<ICollection<ProjectTasksNumberDTO>> GetUserProjectsTasksNumber(int userId);

        // Query 7
        Task<ICollection<ProjectStaticticsDTO>> GetProjectStatictics(int minProjectDescriptionLengthForCounting, int maxProjectTasksCountForCounting);
    }
}
