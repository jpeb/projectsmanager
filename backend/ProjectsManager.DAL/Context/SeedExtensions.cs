﻿using Bogus;
using ProjectsManager.DAL.Entities;
using System.Collections.Generic;
using Task = ProjectsManager.DAL.Entities.Task;

namespace ProjectsManager.DAL.Context
{
    public static class SeedExtensions
    {
        public static void SeedTestData(this ProjectsDbContext context, int teamsNumber, int usersNumber, int projectsNumber, int tasksNumber)
        {
            var teams = GenerateRandomTeams(teamsNumber);
            var users = GenerateRandomUsers(usersNumber, teams);
            var projects = GenerateRandomProjects(projectsNumber, teams, users);
            var tasks = GenerateRandomTasks(tasksNumber, projects, users);

            context.Teams.AddRange(teams);
            context.Users.AddRange(users);
            context.Projects.AddRange(projects);
            context.Tasks.AddRange(tasks);

            context.SaveChanges();
        }

        public static ICollection<Team> GenerateRandomTeams(int number)
        {
            var teamsFake = new Faker<Team>()
               .RuleFor(pi => pi.Name, f => f.Random.Words(f.Random.Number(1, 5)));

            return teamsFake.Generate(number);
        }

        public static ICollection<User> GenerateRandomUsers(int number, IEnumerable<Team> teams)
        {
            var uersFake = new Faker<User>()
               .RuleFor(pi => pi.CreatedAt, f => f.Date.Past(1))
               .RuleFor(pi => pi.Team, f => f.PickRandom(teams).OrNull(f, 0.05f))
               .RuleFor(pi => pi.FirstName, f => f.Name.FirstName())
               .RuleFor(pi => pi.LastName, f => f.Name.LastName())
               .RuleFor(pi => pi.Email, f => f.Internet.Email())
               .RuleFor(pi => pi.BirthDay, f => f.Date.Past(50));

            return uersFake.Generate(number);
        }

        public static ICollection<Project> GenerateRandomProjects(int number, IEnumerable<Team> teams, IEnumerable<User> users)
        {
            var projectFake = new Faker<Project>()
               .RuleFor(pi => pi.Author, f => f.PickRandom(users))
               .RuleFor(pi => pi.Team, f => f.PickRandom(teams))
               .RuleFor(pi => pi.Name, f => f.Lorem.Sentences(1).Trim('.'))
               .RuleFor(pi => pi.Description, f => f.Lorem.Sentences(f.Random.Number(3, 8)))
               .RuleFor(pi => pi.Deadline, f => f.Date.Future(f.Random.Number(1, 3)));

            return projectFake.Generate(number);
        }

        public static ICollection<Task> GenerateRandomTasks(int number, IEnumerable<Project> projects, IEnumerable<User> users)
        {
            TaskState taskState = TaskState.Created;
            var taskFake = new Faker<Task>()
               .RuleFor(pi => pi.Project, f => f.PickRandom(projects))
               .RuleFor(pi => pi.Performer, f => f.PickRandom(users))
               .RuleFor(pi => pi.Name, f => f.Random.Words(f.Random.Number(1, 3)))
               .RuleFor(pi => pi.Description, f => f.Lorem.Sentences(f.Random.Number(1, 2)))
               .RuleFor(pi => pi.Name, f => f.Random.Words(f.Random.Number(1, 5)))
               .RuleFor(pi => pi.Description, f => f.Lorem.Sentences(f.Random.Number(1, 3)))
               .RuleFor(pi => pi.State, f => { taskState = f.Random.Enum<TaskState>(); return taskState; })
               .RuleFor(pi => pi.FinishedAt, f => taskState == TaskState.Finished ? f.Date.Past() : null);

            return taskFake.Generate(number);
        }
    }
}
