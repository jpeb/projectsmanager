﻿using Microsoft.EntityFrameworkCore;
using ProjectsManager.DAL.Entities;
using Task = ProjectsManager.DAL.Entities.Task;

namespace ProjectsManager.DAL.Context
{
    public static class ConfigureExtentions
    {
        public static void Configure(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Task>()
                .HasOne(p => p.Performer)
                .WithMany(t => t.Tasks)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Task>().HasQueryFilter(p => !p.IsDeleted);
            modelBuilder.Entity<Project>().HasQueryFilter(p => !p.IsDeleted);
            modelBuilder.Entity<Team>().HasQueryFilter(p => !p.IsDeleted);
            modelBuilder.Entity<User>().HasQueryFilter(p => !p.IsDeleted);
        }
    }
}
