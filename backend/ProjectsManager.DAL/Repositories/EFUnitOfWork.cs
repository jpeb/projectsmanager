﻿using ProjectsManager.DAL.Context;
using ProjectsManager.DAL.Entities;
using ProjectsManager.DAL.Repositories.Abstract;
using System.Threading.Tasks;
using Task = ProjectsManager.DAL.Entities.Task;

namespace ProjectsManager.DAL.Repositories
{
    public class EFUnitOfWork : IUnitOfWork
    {
        private readonly ProjectsDbContext _db;

        public EFUnitOfWork(ProjectsDbContext databaseContext)
        {
            _db = databaseContext;

            ProjectRepository = new EFRepository<Project>(databaseContext);
            TaskRepository = new EFRepository<Task>(databaseContext);
            TeamRepository = new EFRepository<Team>(databaseContext);
            UserRepository = new EFRepository<User>(databaseContext);
        }

        public IRepository<Project> ProjectRepository { get; }
        public IRepository<Task> TaskRepository { get; }
        public IRepository<Team> TeamRepository { get; }
        public IRepository<User> UserRepository { get; }

        public async Task<int> SaveChangesAsync()
        {
            return await _db.SaveChangesAsync();
        }

        public async ValueTask DisposeAsync()
        {
            await _db.DisposeAsync();
        }
    }
}
