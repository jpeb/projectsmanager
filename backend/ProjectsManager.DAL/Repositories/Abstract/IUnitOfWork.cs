﻿using ProjectsManager.DAL.Entities;
using System;
using System.Threading.Tasks;
using Task = ProjectsManager.DAL.Entities.Task;

namespace ProjectsManager.DAL.Repositories.Abstract
{
    public interface IUnitOfWork : IAsyncDisposable
    {
        IRepository<Project> ProjectRepository { get; }
        IRepository<Task> TaskRepository { get; }
        IRepository<Team> TeamRepository { get; }
        IRepository<User> UserRepository { get; }

        Task<int> SaveChangesAsync();
    }
}
