import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subject, takeUntil } from 'rxjs';
import { Project } from 'src/app/core/models/project/project';
import { ProjectsService } from 'src/app/core/services/projects.service';
import { SnackBarService } from 'src/app/core/services/shack-bar.service';

@Component({
  templateUrl: './project-delete-dialog.component.html',
  styleUrls: ['./project-delete-dialog.component.scss']
})
export class ProjectDeleteDialogComponent implements OnInit, OnDestroy {
    public projectId!: number;
    public loading = false;

    private unsubscribe$ = new Subject<void>();

    constructor(
        private dialogRef: MatDialogRef<ProjectDeleteDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: { projectId: number },
        private projectsService: ProjectsService,
        private snackBarService: SnackBarService
    ) {}

    ngOnInit(): void {
        this.projectId = this.data.projectId;
    }

    public deleteProject() {
        this.loading = true;

        this.projectsService
            .deleteProject(this.projectId)
            .pipe(takeUntil(this.unsubscribe$)).subscribe({
                next: () => {
                    this.snackBarService.showUsualMessage('Successfully deleted');
                    this.loading = false;
                    this.dialogRef.close(true);
                },
                error: (error) => {
                    this.snackBarService.showErrorMessage(error?.message || "An error occurred while deleting")
                    this.loading = false;
                    this.dialogRef.close();
                } 
            });
    }

    ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
}
