import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Project } from 'src/app/core/models/project/project';

@Component({
    selector: 'app-project-card',
    templateUrl: './project-card.component.html',
    styleUrls: ['./project-card.component.scss']
})
export class ProjectCardComponent implements OnInit {
    @Input() public project!: Project;
    @Output() onDelete = new EventEmitter<number>();
    
    constructor() {}

    ngOnInit(): void {
    }

    public deleteProject(){
        this.onDelete.next(this.project.id);
    }
}
