import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subject, takeUntil } from 'rxjs';
import { Project } from 'src/app/core/models/project/project';
import { ProjectsService } from 'src/app/core/services/projects.service';
import { SnackBarService } from 'src/app/core/services/shack-bar.service';
import { ProjectDeleteDialogComponent } from './project-delete-dialog/project-delete-dialog.component';

@Component({
    templateUrl: './projects-list.component.html',
    styleUrls: ['./projects-list.component.scss']
})
export class ProjectsListComponent implements OnInit, OnDestroy {

    public projects: Project[] = [];
    public loading = false;

    private unsubscribe$ = new Subject<void>();

    constructor(private dialog: MatDialog, 
        private projectsService: ProjectsService, 
        private snackBarService: SnackBarService) {}

    ngOnInit(): void {
        this.loadProjects();
    }

    public loadProjects() {
        this.loading = true;
        this.projectsService
            .getProjects()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe({
                next: (resp) => {
                    this.loading = false;
                    this.projects = resp.body ?? [];
                },
                error: (error) => {
                    this.snackBarService.showErrorMessage(error?.message || "An error occurred while updating")
                    this.loading = false;
                }
            });
    }

    public deleteProject(id: number) {
        this.dialog.open(ProjectDeleteDialogComponent, {
            data: { projectId: id },
            autoFocus: false,
            backdropClass: 'dialog-backdrop'
        })
            .afterClosed()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe({
                next: (isDeleted) => {
                    if (isDeleted)
                        this.projects = this.projects.filter(e => e.id !== id);
                }
            });
    }

    ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
}
