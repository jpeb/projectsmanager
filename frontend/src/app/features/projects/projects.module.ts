import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { MaterialModule } from 'src/app/shared/material.module';
import { ProjectsListComponent } from './projects-list/projects-list.component';
import { ProjectCardComponent } from './projects-list/project-card/project-card.component';
import { NewProjectComponent } from './new-project/new-project.component';
import { UpdateProjectComponent } from './update-project/update-project.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ProjectDeleteDialogComponent } from './projects-list/project-delete-dialog/project-delete-dialog.component';
import { ProjectDetailsComponent } from './project-details/project-details.component';
import { UnsavedChangesWarnGuard } from 'src/app/core/guards/unsaved-changes-warn.guard';
import { PipesModule } from 'src/app/shared/pipes/pipes.module';

const routes: Routes = [
    {
        path: '', component: ProjectsListComponent
    },
    {
        path: 'new', component: NewProjectComponent, canDeactivate: [UnsavedChangesWarnGuard]
    },
    {
        path: ':id', component: ProjectDetailsComponent
    },
    {
        path: ':id/edit', component: UpdateProjectComponent, canDeactivate: [UnsavedChangesWarnGuard]
    }
];

@NgModule({
    declarations: [
        ProjectsListComponent,
        ProjectCardComponent,
        NewProjectComponent,
        UpdateProjectComponent,
        ProjectDeleteDialogComponent,
        ProjectDetailsComponent
    ],
    imports: [
        CommonModule,
        MaterialModule,
        RouterModule.forChild(routes),
        ReactiveFormsModule,
        PipesModule
    ]
})
export class ProjectsModule {}
