import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Subject, takeUntil, combineLatest } from 'rxjs';
import { Project } from 'src/app/core/models/project/project';
import { UpdateProject } from 'src/app/core/models/project/update-project';
import { TeamName } from 'src/app/core/models/team/team-name';
import { UserName } from 'src/app/core/models/user/user-name';
import { ProjectsService } from 'src/app/core/services/projects.service';
import { SnackBarService } from 'src/app/core/services/shack-bar.service';
import { TeamsService } from 'src/app/core/services/teams.service';
import { UsersService } from 'src/app/core/services/users.service';
import { NewProject } from 'src/app/core/models/project/new-project';
import { UnsavedChangesComponent } from 'src/app/core/models/components/unsaved-changes-component';

@Component({
  templateUrl: './new-project.component.html',
  styleUrls: ['./new-project.component.scss']
})
export class NewProjectComponent implements OnInit, OnDestroy, UnsavedChangesComponent {
    public project!: Project;
    public teamsOptions: TeamName[] = []
    public usersOptions: UserName[] = []

    projectForm!: FormGroup;

    public loading = false;
    public saved = false;

    private unsubscribe$ = new Subject<void>();

    constructor(
        private location: Location,
        private projectsService: ProjectsService,
        private usersService: UsersService,
        private teamsService: TeamsService,
        private snackBarService: SnackBarService) {}

    ngOnInit(): void {

        this.projectForm = new FormGroup({
            name: new FormControl('', [Validators.required]),
            description: new FormControl(''),
            deadline: new FormControl('', [Validators.required]),
            authorId: new FormControl('', [Validators.required]),
            teamId: new FormControl('', [Validators.required]),
        });

        combineLatest([this.usersService.getUsersNames(), this.teamsService.getTeamsNames()])
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe({
                next: ([usersOptionsResp, teamsOptionsResp]) => {
                    this.usersOptions = usersOptionsResp.body ?? [];
                    this.teamsOptions = teamsOptionsResp.body ?? [];
                },
                error: (error) => {
                    this.snackBarService.showErrorMessage(error?.message || "An error occurred while loading")
                    this.loading = false;
                }
            })
    }

    public saveProject() {
        let newProject = this.projectForm.value as NewProject;

        this.loading = true;
        this.projectsService.createProject(newProject)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe({
                next: (project) => {
                    this.loading = false;
                    this.saved = true;
                    this.goBack();
                    this.snackBarService.showUsualMessage('Successfully created project with ID=' + project?.body?.id);
                },
                error: (error) => {
                    this.snackBarService.showErrorMessage(error?.message || "An error occurred while creating")
                    this.loading = false;
                }
            });
    }

    public goBack = () => this.location.back();

    public hasUnsavedChanges(): boolean {
        return !this.saved && this.projectForm.touched;
    };

    ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
}
