import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { combineLatest, Subject, takeUntil } from 'rxjs';
import { Project } from 'src/app/core/models/project/project';
import { UpdateProject } from 'src/app/core/models/project/update-project';
import { TeamName } from 'src/app/core/models/team/team-name';
import { UserName } from 'src/app/core/models/user/user-name';
import { ProjectsService } from 'src/app/core/services/projects.service';
import { SnackBarService } from 'src/app/core/services/shack-bar.service';
import { TeamsService } from 'src/app/core/services/teams.service';
import { UsersService } from 'src/app/core/services/users.service';
import { UnsavedChangesComponent } from 'src/app/core/models/components/unsaved-changes-component';

@Component({
    templateUrl: './update-project.component.html',
    styleUrls: ['./update-project.component.scss']
})
export class UpdateProjectComponent implements OnInit, OnDestroy, UnsavedChangesComponent {
    public project!: Project;
    public teamsOptions: TeamName[] = []
    public usersOptions: UserName[] = []

    projectForm!: FormGroup;

    public loading = false;
    public saved = false;

    private unsubscribe$ = new Subject<void>();

    constructor(
        private location: Location,
        private route: ActivatedRoute,
        private projectsService: ProjectsService,
        private usersService: UsersService,
        private teamsService: TeamsService,
        private snackBarService: SnackBarService) {}

    ngOnInit(): void {

        this.projectForm = new FormGroup({
            name: new FormControl('', [Validators.required]),
            description: new FormControl(''),
            deadline: new FormControl('', [Validators.required]),
            authorId: new FormControl('', [Validators.required]),
            teamId: new FormControl('', [Validators.required]),
        });

        this.route.params
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(params => {
                let id = params['id'] as number;
                this.loadProject(id);
            });

        combineLatest([this.usersService.getUsersNames(), this.teamsService.getTeamsNames()])
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe({
                next: ([usersOptionsResp, teamsOptionsResp]) => {
                    this.usersOptions = usersOptionsResp.body ?? [];
                    this.teamsOptions = teamsOptionsResp.body ?? [];
                },
                error: (error) => {
                    this.snackBarService.showErrorMessage(error?.message || "An error occurred while loading")
                    this.loading = false;
                }
            })
    }

    public loadProject(id: number) {
        this.loading = true;
        this.projectsService
            .getProject(id)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe({
                next: (resp) => {
                    this.loading = false;
                    this.project = resp;
                    this.projectForm.patchValue(this.project);
                },
                error: (error) => {
                    this.loading = false;
                    this.goBack();
                    this.snackBarService.showErrorMessage(error?.message || "An error occurred while loading")
                }
            });
    }

    public saveProject() {
        let newProject = this.projectForm.value as UpdateProject;

        if (!this.project?.id) return;

        this.loading = true;
        this.projectsService.updateProject(this.project.id, newProject)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe({
                next: () => {
                    this.loading = false;
                    this.saved = true;
                    this.goBack();
                    this.snackBarService.showUsualMessage('Successfully updated');
                },
                error: (error) => {
                    this.snackBarService.showErrorMessage(error?.message || "An error occurred while updating")
                    this.loading = false;
                }
            });
    }

    public goBack = () => this.location.back();

    public hasUnsavedChanges(): boolean {
        return !this.saved && this.projectForm.touched;
    };

    ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
}
