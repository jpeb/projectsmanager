import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
import { Project } from 'src/app/core/models/project/project';
import { ProjectsService } from 'src/app/core/services/projects.service';
import { SnackBarService } from 'src/app/core/services/shack-bar.service';
import { Location } from '@angular/common'

@Component({
    templateUrl: './project-details.component.html',
    styleUrls: ['./project-details.component.scss']
})
export class ProjectDetailsComponent implements OnInit, OnDestroy {
    public project: Project | null = null;
    public loading = false;

    private unsubscribe$ = new Subject<void>();

    constructor(
        private location: Location,
        private route: ActivatedRoute,
        private projectsService: ProjectsService,
        private snackBarService: SnackBarService) {}

    ngOnInit(): void {
        this.route.params
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(params => {
                let id = params['id'] as number;
                this.loadProject(id);
            });
    }

    public loadProject(id: number) {
        this.loading = true;
        this.projectsService
            .getProject(id)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe({
                next: (resp) => {
                    this.loading = false;
                    this.project = resp;
                },
                error: (error) => {
                    this.loading = false;
                    this.goBack();
                    this.snackBarService.showErrorMessage(error?.message || "An error occurred while loading")
                }
            });
    }

    public goBack = () => this.location.back();

    ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
}
