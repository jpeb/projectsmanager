import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Subject, takeUntil, combineLatest } from 'rxjs';
import { Team } from 'src/app/core/models/team/team';
import { UpdateTeam } from 'src/app/core/models/team/update-team';
import { TeamName } from 'src/app/core/models/team/team-name';
import { UserName } from 'src/app/core/models/user/user-name';
import { TeamsService } from 'src/app/core/services/teams.service';
import { SnackBarService } from 'src/app/core/services/shack-bar.service';
import { UsersService } from 'src/app/core/services/users.service';
import { NewTeam } from 'src/app/core/models/team/new-team';
import { UnsavedChangesComponent } from 'src/app/core/models/components/unsaved-changes-component';

@Component({
  templateUrl: './new-team.component.html',
  styleUrls: ['./new-team.component.scss']
})
export class NewTeamComponent implements OnInit, OnDestroy, UnsavedChangesComponent {
    public team!: Team;
    public teamsOptions: TeamName[] = []
    public usersOptions: UserName[] = []

    teamForm!: FormGroup;

    public loading = false;
    public saved = false;

    private unsubscribe$ = new Subject<void>();

    constructor(
        private location: Location,
        private teamsService: TeamsService,
        private snackBarService: SnackBarService) {}

    ngOnInit(): void {

        this.teamForm = new FormGroup({
            name: new FormControl('', [Validators.required]),
        });
    }

    public saveTeam() {
        let newTeam = this.teamForm.value as NewTeam;

        this.loading = true;
        this.teamsService.createTeam(newTeam)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe({
                next: (team) => {
                    this.loading = false;
                    this.saved = true;
                    this.goBack();
                    this.snackBarService.showUsualMessage('Successfully created team with ID=' + team?.body?.id);
                },
                error: (error) => {
                    this.snackBarService.showErrorMessage(error?.message || "An error occurred while creating")
                    this.loading = false;
                }
            });
    }

    public goBack = () => this.location.back();

    public hasUnsavedChanges(): boolean {
        return !this.saved && this.teamForm.touched;
    };

    ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
}
