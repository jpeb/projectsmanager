import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { combineLatest, Subject, takeUntil } from 'rxjs';
import { Team } from 'src/app/core/models/team/team';
import { UpdateTeam } from 'src/app/core/models/team/update-team';
import { TeamName } from 'src/app/core/models/team/team-name';
import { UserName } from 'src/app/core/models/user/user-name';
import { SnackBarService } from 'src/app/core/services/shack-bar.service';
import { UnsavedChangesComponent } from 'src/app/core/models/components/unsaved-changes-component';
import { TeamsService } from 'src/app/core/services/teams.service';

@Component({
    templateUrl: './update-team.component.html',
    styleUrls: ['./update-team.component.scss']
})
export class UpdateTeamComponent implements OnInit, OnDestroy, UnsavedChangesComponent {
    public team!: Team;
    teamForm!: FormGroup;

    public loading = false;
    public saved = false;

    private unsubscribe$ = new Subject<void>();

    constructor(
        private location: Location,
        private route: ActivatedRoute,
        private teamsService: TeamsService,
        private snackBarService: SnackBarService) {}

    ngOnInit(): void {

        this.teamForm = new FormGroup({
            name: new FormControl('', [Validators.required]),
        });

        this.route.params
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(params => {
                let id = params['id'] as number;
                this.loadTeam(id);
            });
    }

    public loadTeam(id: number) {
        this.loading = true;
        this.teamsService
            .getTeam(id)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe({
                next: (resp) => {
                    this.loading = false;
                    this.team = resp;
                    this.teamForm.patchValue(this.team);
                },
                error: (error) => {
                    this.loading = false;
                    this.goBack();
                    this.snackBarService.showErrorMessage(error?.message || "An error occurred while loading")
                }
            });
    }

    public saveTeam() {
        let newTeam = this.teamForm.value as UpdateTeam;

        if (!this.team?.id) return;

        this.loading = true;
        this.teamsService.updateTeam(this.team.id, newTeam)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe({
                next: () => {
                    this.loading = false;
                    this.saved = true;
                    this.goBack();
                    this.snackBarService.showUsualMessage('Successfully updated');
                },
                error: (error) => {
                    this.snackBarService.showErrorMessage(error?.message || "An error occurred while updating")
                    this.loading = false;
                }
            });
    }

    public goBack = () => this.location.back();

    public hasUnsavedChanges(): boolean {
        return !this.saved && this.teamForm.touched;
    };

    ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
}
