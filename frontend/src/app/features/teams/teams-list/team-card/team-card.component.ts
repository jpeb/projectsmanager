import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Team } from 'src/app/core/models/team/team';

@Component({
    selector: 'app-team-card',
    templateUrl: './team-card.component.html',
    styleUrls: ['./team-card.component.scss']
})
export class TeamCardComponent implements OnInit {
    @Input() public team!: Team;
    @Output() onDelete = new EventEmitter<number>();
    
    constructor() {}

    ngOnInit(): void {
    }

    public deleteTeam(){
        this.onDelete.next(this.team.id);
    }
}
