import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamDeleteDialogComponent } from './team-delete-dialog.component';

describe('TeamDeleteDialogComponent', () => {
  let component: TeamDeleteDialogComponent;
  let fixture: ComponentFixture<TeamDeleteDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TeamDeleteDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamDeleteDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
