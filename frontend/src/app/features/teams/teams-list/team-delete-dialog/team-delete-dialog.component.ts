import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subject, takeUntil } from 'rxjs';
import { Team } from 'src/app/core/models/team/team';
import { TeamsService } from 'src/app/core/services/teams.service';
import { SnackBarService } from 'src/app/core/services/shack-bar.service';

@Component({
  templateUrl: './team-delete-dialog.component.html',
  styleUrls: ['./team-delete-dialog.component.scss']
})
export class TeamDeleteDialogComponent implements OnInit, OnDestroy {
    public teamId!: number;
    public loading = false;

    private unsubscribe$ = new Subject<void>();

    constructor(
        private dialogRef: MatDialogRef<TeamDeleteDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: { teamId: number },
        private teamsService: TeamsService,
        private snackBarService: SnackBarService
    ) {}

    ngOnInit(): void {
        this.teamId = this.data.teamId;
    }

    public deleteTeam() {
        this.loading = true;

        this.teamsService
            .deleteTeam(this.teamId)
            .pipe(takeUntil(this.unsubscribe$)).subscribe({
                next: () => {
                    this.snackBarService.showUsualMessage('Successfully deleted');
                    this.loading = false;
                    this.dialogRef.close(true);
                },
                error: (error) => {
                    this.snackBarService.showErrorMessage(error?.message || "An error occurred while deleting")
                    this.loading = false;
                    this.dialogRef.close();
                } 
            });
    }

    ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
}
