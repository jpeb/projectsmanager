import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subject, takeUntil } from 'rxjs';
import { Team } from 'src/app/core/models/team/team';
import { TeamsService } from 'src/app/core/services/teams.service';
import { SnackBarService } from 'src/app/core/services/shack-bar.service';
import { TeamDeleteDialogComponent } from './team-delete-dialog/team-delete-dialog.component';

@Component({
    templateUrl: './teams-list.component.html',
    styleUrls: ['./teams-list.component.scss']
})
export class TeamsListComponent implements OnInit, OnDestroy {

    public teams: Team[] = [];
    public loading = false;

    private unsubscribe$ = new Subject<void>();

    constructor(private dialog: MatDialog, 
        private teamsService: TeamsService, 
        private snackBarService: SnackBarService) {}

    ngOnInit(): void {
        this.loadTeams();
    }

    public loadTeams() {
        this.loading = true;
        this.teamsService
            .getTeams()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe({
                next: (resp) => {
                    this.loading = false;
                    this.teams = resp.body ?? [];
                },
                error: (error) => {
                    this.snackBarService.showErrorMessage(error?.message || "An error occurred while updating")
                    this.loading = false;
                }
            });
    }

    public deleteTeam(id: number) {
        this.dialog.open(TeamDeleteDialogComponent, {
            data: { teamId: id },
            autoFocus: false,
            backdropClass: 'dialog-backdrop'
        })
            .afterClosed()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe({
                next: (isDeleted) => {
                    if (isDeleted)
                        this.teams = this.teams.filter(e => e.id !== id);
                }
            });
    }

    ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
}
