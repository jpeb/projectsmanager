import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { MaterialModule } from 'src/app/shared/material.module';
import { TeamsListComponent } from './teams-list/teams-list.component';
import { NewTeamComponent } from './new-team/new-team.component';
import { UpdateTeamComponent } from './update-team/update-team.component';
import { ReactiveFormsModule } from '@angular/forms';
import { TeamDetailsComponent } from './team-details/team-details.component';
import { UnsavedChangesWarnGuard } from 'src/app/core/guards/unsaved-changes-warn.guard';
import { PipesModule } from 'src/app/shared/pipes/pipes.module';
import { TeamCardComponent } from './teams-list/team-card/team-card.component';
import { TeamDeleteDialogComponent } from './teams-list/team-delete-dialog/team-delete-dialog.component';

const routes: Routes = [
    {
        path: '', component: TeamsListComponent
    },
    {
        path: 'new', component: NewTeamComponent, canDeactivate: [UnsavedChangesWarnGuard]
    },
    {
        path: ':id', component: TeamDetailsComponent
    },
    {
        path: ':id/edit', component: UpdateTeamComponent, canDeactivate: [UnsavedChangesWarnGuard]
    }
];

@NgModule({
    declarations: [
        TeamsListComponent,
        TeamCardComponent,
        NewTeamComponent,
        UpdateTeamComponent,
        TeamDeleteDialogComponent,
        TeamDetailsComponent
    ],
    imports: [
        CommonModule,
        MaterialModule,
        RouterModule.forChild(routes),
        ReactiveFormsModule,
        PipesModule
    ]
})
export class TeamsModule {}
