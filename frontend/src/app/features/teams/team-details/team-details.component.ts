import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
import { Team } from 'src/app/core/models/team/team';
import { TeamsService } from 'src/app/core/services/teams.service';
import { SnackBarService } from 'src/app/core/services/shack-bar.service';
import { Location } from '@angular/common'

@Component({
    templateUrl: './team-details.component.html',
    styleUrls: ['./team-details.component.scss']
})
export class TeamDetailsComponent implements OnInit, OnDestroy {
    public team: Team | null = null;
    public loading = false;

    private unsubscribe$ = new Subject<void>();

    constructor(
        private location: Location,
        private route: ActivatedRoute,
        private teamsService: TeamsService,
        private snackBarService: SnackBarService) {}

    ngOnInit(): void {
        this.route.params
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(params => {
                let id = params['id'] as number;
                this.loadTeam(id);
            });
    }

    public loadTeam(id: number) {
        this.loading = true;
        this.teamsService
            .getTeam(id)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe({
                next: (resp) => {
                    this.loading = false;
                    this.team = resp;
                },
                error: (error) => {
                    this.loading = false;
                    this.goBack();
                    this.snackBarService.showErrorMessage(error?.message || "An error occurred while loading")
                }
            });
    }

    public goBack = () => this.location.back();

    ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
}
