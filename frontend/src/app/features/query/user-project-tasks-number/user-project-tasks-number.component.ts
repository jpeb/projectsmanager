import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subject, takeUntil } from 'rxjs';
import { ProjectTasksNumber } from 'src/app/core/models/project/project-tasks-number';
import { TaskState } from 'src/app/core/models/task/task-state';
import { UserName } from 'src/app/core/models/user/user-name';
import { QueriesService } from 'src/app/core/services/queries.service';
import { SnackBarService } from 'src/app/core/services/shack-bar.service';
import { UsersService } from 'src/app/core/services/users.service';

@Component({
  templateUrl: './user-project-tasks-number.component.html',
  styleUrls: ['./user-project-tasks-number.component.scss']
})
export class UserProjectTasksNumberComponent implements OnInit, OnDestroy {

    public userProjects?: ProjectTasksNumber[];
    public usersOptions: UserName[] = [];

    queryForm!: FormGroup;
    public loading = false;

    private unsubscribe$ = new Subject<void>();

    constructor(
        private usersService: UsersService,
        private queriesService: QueriesService,
        private snackBarService: SnackBarService) {}

    ngOnInit(): void {

        this.queryForm = new FormGroup({
            userId: new FormControl('', [Validators.required]),
        });

        this.usersService.getUsersNames()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe({
                next: (usersOptionsResp) => {
                    this.usersOptions = usersOptionsResp.body ?? [];
                },
                error: (error) => {
                    this.snackBarService.showErrorMessage(error?.message || "An error occurred while loading")
                    this.loading = false;
                }
            });
    }

    executeQuery(){
        this.loading = true;
        this.queriesService
            .getUserProjectTasksNumber(this.queryForm.get('userId')?.value as number)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe({
                next: (resp) => {
                    this.loading = false;
                    this.userProjects = resp;
                },
                error: (error) => {
                    this.snackBarService.showErrorMessage(error?.message || "An error occurred while loading")
                    this.loading = false;
                }
            });
    }

    ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
}
