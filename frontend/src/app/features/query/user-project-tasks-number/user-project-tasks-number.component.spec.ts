import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserProjectTasksNumberComponent } from './user-project-tasks-number.component';

describe('UserProjectTasksNumberComponent', () => {
  let component: UserProjectTasksNumberComponent;
  let fixture: ComponentFixture<UserProjectTasksNumberComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserProjectTasksNumberComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserProjectTasksNumberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
