import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { UserProjectTasksNumberComponent } from './user-project-tasks-number/user-project-tasks-number.component';
import { MaterialModule } from 'src/app/shared/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { PipesModule } from 'src/app/shared/pipes/pipes.module';

const routes: Routes = [
    { path: 'userProjectTasksNumber', component: UserProjectTasksNumberComponent }
];

@NgModule({
    declarations: [
        UserProjectTasksNumberComponent
    ],
    imports: [
        CommonModule,
        MaterialModule,
        ReactiveFormsModule,
        PipesModule,
        RouterModule.forChild(routes)
    ]
})
export class QueryModule {}
