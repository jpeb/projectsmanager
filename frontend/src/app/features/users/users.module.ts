import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { MaterialModule } from 'src/app/shared/material.module';
import { UsersListComponent } from './users-list/users-list.component';
import { UserCardComponent } from './users-list/user-card/user-card.component';
import { NewUserComponent } from './new-user/new-user.component';
import { UpdateUserComponent } from './update-user/update-user.component';
import { ReactiveFormsModule } from '@angular/forms';
import { UserDeleteDialogComponent } from './users-list/user-delete-dialog/user-delete-dialog.component';
import { UserDetailsComponent } from './user-details/user-details.component';
import { UnsavedChangesWarnGuard } from 'src/app/core/guards/unsaved-changes-warn.guard';
import { PipesModule } from 'src/app/shared/pipes/pipes.module';

const routes: Routes = [
    {
        path: '', component: UsersListComponent
    },
    {
        path: 'new', component: NewUserComponent, canDeactivate: [UnsavedChangesWarnGuard]
    },
    {
        path: ':id', component: UserDetailsComponent
    },
    {
        path: ':id/edit', component: UpdateUserComponent, canDeactivate: [UnsavedChangesWarnGuard]
    }
];

@NgModule({
    declarations: [
        UsersListComponent,
        UserCardComponent,
        NewUserComponent,
        UpdateUserComponent,
        UserDeleteDialogComponent,
        UserDetailsComponent
    ],
    imports: [
        CommonModule,
        MaterialModule,
        RouterModule.forChild(routes),
        ReactiveFormsModule,
        PipesModule
    ]
})
export class UsersModule {}
