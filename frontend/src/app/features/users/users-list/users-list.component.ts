import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subject, takeUntil } from 'rxjs';
import { User } from 'src/app/core/models/user/user';
import { UsersService } from 'src/app/core/services/users.service';
import { SnackBarService } from 'src/app/core/services/shack-bar.service';
import { UserDeleteDialogComponent } from './user-delete-dialog/user-delete-dialog.component';

@Component({
    templateUrl: './users-list.component.html',
    styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit, OnDestroy {

    public users: User[] = [];
    public loading = false;

    private unsubscribe$ = new Subject<void>();

    constructor(private dialog: MatDialog, 
        private usersService: UsersService, 
        private snackBarService: SnackBarService) {}

    ngOnInit(): void {
        this.loadUsers();
    }

    public loadUsers() {
        this.loading = true;
        this.usersService
            .getUsers()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe({
                next: (resp) => {
                    this.loading = false;
                    this.users = resp.body ?? [];
                },
                error: (error) => {
                    this.snackBarService.showErrorMessage(error?.message || "An error occurred while updating")
                    this.loading = false;
                }
            });
    }

    public deleteUser(id: number) {
        this.dialog.open(UserDeleteDialogComponent, {
            data: { userId: id },
            autoFocus: false,
            backdropClass: 'dialog-backdrop'
        })
            .afterClosed()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe({
                next: (isDeleted) => {
                    if (isDeleted)
                        this.users = this.users.filter(e => e.id !== id);
                }
            });
    }

    ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
}
