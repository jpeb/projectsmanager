import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subject, takeUntil } from 'rxjs';
import { User } from 'src/app/core/models/user/user';
import { UsersService } from 'src/app/core/services/users.service';
import { SnackBarService } from 'src/app/core/services/shack-bar.service';

@Component({
  templateUrl: './user-delete-dialog.component.html',
  styleUrls: ['./user-delete-dialog.component.scss']
})
export class UserDeleteDialogComponent implements OnInit, OnDestroy {
    public userId!: number;
    public loading = false;

    private unsubscribe$ = new Subject<void>();

    constructor(
        private dialogRef: MatDialogRef<UserDeleteDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: { userId: number },
        private usersService: UsersService,
        private snackBarService: SnackBarService
    ) {}

    ngOnInit(): void {
        this.userId = this.data.userId;
    }

    public deleteUser() {
        this.loading = true;

        this.usersService
            .deleteUser(this.userId)
            .pipe(takeUntil(this.unsubscribe$)).subscribe({
                next: () => {
                    this.snackBarService.showUsualMessage('Successfully deleted');
                    this.loading = false;
                    this.dialogRef.close(true);
                },
                error: (error) => {
                    this.snackBarService.showErrorMessage(error?.message || "An error occurred while deleting")
                    this.loading = false;
                    this.dialogRef.close();
                } 
            });
    }

    ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
}
