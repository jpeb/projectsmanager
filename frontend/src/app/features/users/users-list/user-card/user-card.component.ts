import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { User } from 'src/app/core/models/user/user';

@Component({
    selector: 'app-user-card',
    templateUrl: './user-card.component.html',
    styleUrls: ['./user-card.component.scss']
})
export class UserCardComponent implements OnInit {
    @Input() public user!: User;
    @Output() onDelete = new EventEmitter<number>();
    
    constructor() {}

    ngOnInit(): void {
    }

    public deleteUser(){
        this.onDelete.next(this.user.id);
    }
}
