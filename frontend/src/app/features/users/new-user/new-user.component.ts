import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Subject, takeUntil, combineLatest } from 'rxjs';
import { User } from 'src/app/core/models/user/user';
import { TeamName } from 'src/app/core/models/team/team-name';
import { UserName } from 'src/app/core/models/user/user-name';
import { UsersService } from 'src/app/core/services/users.service';
import { SnackBarService } from 'src/app/core/services/shack-bar.service';
import { TeamsService } from 'src/app/core/services/teams.service';
import { NewUser } from 'src/app/core/models/user/new-user';
import { UnsavedChangesComponent } from 'src/app/core/models/components/unsaved-changes-component';

@Component({
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.scss']
})
export class NewUserComponent implements OnInit, OnDestroy, UnsavedChangesComponent {
    public user!: User;
    public teamsOptions: TeamName[] = []
    public usersOptions: UserName[] = []

    userForm!: FormGroup;

    public loading = false;
    public saved = false;

    private unsubscribe$ = new Subject<void>();

    constructor(
        private location: Location,
        private usersService: UsersService,
        private teamsService: TeamsService,
        private snackBarService: SnackBarService) {}

    ngOnInit(): void {

        this.userForm = new FormGroup({
            firstName: new FormControl('', [Validators.required]),
            lastName: new FormControl(''),
            email: new FormControl('', [Validators.required, Validators.email]),
            teamId: new FormControl(null),
            birthDay: new FormControl('', [Validators.required]),
        });

        this.teamsService.getTeamsNames()
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe({
            next: (teamsOptionsResp) => {
                this.teamsOptions = teamsOptionsResp.body ?? [];
            },
            error: (error) => {
                this.snackBarService.showErrorMessage(error?.message || "An error occurred while loading")
                this.loading = false;
            }
        })
    }

    public saveUser() {
        let newUser = this.userForm.value as NewUser;

        this.loading = true;
        this.usersService.createUser(newUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe({
                next: (user) => {
                    this.loading = false;
                    this.saved = true;
                    this.goBack();
                    this.snackBarService.showUsualMessage('Successfully created user with ID=' + user?.body?.id);
                },
                error: (error) => {
                    this.snackBarService.showErrorMessage(error?.message || "An error occurred while creating")
                    this.loading = false;
                }
            });
    }

    public goBack = () => this.location.back();

    public hasUnsavedChanges(): boolean {
        return !this.saved && this.userForm.touched;
    };

    ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
}
