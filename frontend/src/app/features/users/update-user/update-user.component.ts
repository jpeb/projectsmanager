import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { combineLatest, Subject, takeUntil } from 'rxjs';
import { User } from 'src/app/core/models/user/user';
import { UpdateUser } from 'src/app/core/models/user/update-user';
import { TeamName } from 'src/app/core/models/team/team-name';
import { SnackBarService } from 'src/app/core/services/shack-bar.service';
import { TeamsService } from 'src/app/core/services/teams.service';
import { UnsavedChangesComponent } from 'src/app/core/models/components/unsaved-changes-component';
import { UsersService } from 'src/app/core/services/users.service';

@Component({
    templateUrl: './update-user.component.html',
    styleUrls: ['./update-user.component.scss']
})
export class UpdateUserComponent implements OnInit, OnDestroy, UnsavedChangesComponent {
    public user!: User;
    public teamsOptions: TeamName[] = []

    userForm!: FormGroup;

    public loading = false;
    public saved = false;

    private unsubscribe$ = new Subject<void>();

    constructor(
        private location: Location,
        private route: ActivatedRoute,
        private usersService: UsersService,
        private teamsService: TeamsService,
        private snackBarService: SnackBarService) {}

    ngOnInit(): void {

        this.userForm = new FormGroup({
            firstName: new FormControl('', [Validators.required]),
            lastName: new FormControl(''),
            email: new FormControl('', [Validators.required, Validators.email]),
            teamId: new FormControl(''),
            birthDay: new FormControl('', [Validators.required]),
        });

        this.route.params
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(params => {
                let id = params['id'] as number;
                this.loadUser(id);
            });

        this.teamsService.getTeamsNames()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe({
                next: (teamsOptionsResp) => {
                    this.teamsOptions = teamsOptionsResp.body ?? [];
                },
                error: (error) => {
                    this.snackBarService.showErrorMessage(error?.message || "An error occurred while loading")
                    this.loading = false;
                }
            })
    }

    public loadUser(id: number) {
        this.loading = true;
        this.usersService
            .getUser(id)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe({
                next: (resp) => {
                    this.loading = false;
                    this.user = resp;
                    this.userForm.patchValue(this.user);
                },
                error: (error) => {
                    this.loading = false;
                    this.goBack();
                    this.snackBarService.showErrorMessage(error?.message || "An error occurred while loading")
                }
            });
    }

    public saveUser() {
        let newUser = this.userForm.value as UpdateUser;

        if (!this.user?.id) return;

        this.loading = true;
        this.usersService.updateUser(this.user.id, newUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe({
                next: () => {
                    this.loading = false;
                    this.saved = true;
                    this.goBack();
                    this.snackBarService.showUsualMessage('Successfully updated');
                },
                error: (error) => {
                    this.snackBarService.showErrorMessage(error?.message || "An error occurred while updating")
                    this.loading = false;
                }
            });
    }

    public goBack = () => this.location.back();

    public hasUnsavedChanges(): boolean {
        return !this.saved && this.userForm.touched;
    };

    ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
}
