import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
import { Task } from 'src/app/core/models/task/task';
import { TasksService } from 'src/app/core/services/tasks.service';
import { SnackBarService } from 'src/app/core/services/shack-bar.service';
import { Location } from '@angular/common'

@Component({
    templateUrl: './task-details.component.html',
    styleUrls: ['./task-details.component.scss']
})
export class TaskDetailsComponent implements OnInit, OnDestroy {
    public task: Task | null = null;
    public loading = false;

    private unsubscribe$ = new Subject<void>();

    constructor(
        private location: Location,
        private route: ActivatedRoute,
        private tasksService: TasksService,
        private snackBarService: SnackBarService) {}

    ngOnInit(): void {
        this.route.params
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(params => {
                let id = params['id'] as number;
                this.loadTask(id);
            });
    }

    public loadTask(id: number) {
        this.loading = true;
        this.tasksService
            .getTask(id)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe({
                next: (resp) => {
                    this.loading = false;
                    this.task = resp;
                },
                error: (error) => {
                    this.loading = false;
                    this.goBack();
                    this.snackBarService.showErrorMessage(error?.message || "An error occurred while loading")
                }
            });
    }

    public goBack = () => this.location.back();

    ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
}
