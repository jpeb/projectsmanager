import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { Subject, takeUntil, combineLatest } from 'rxjs';
import { Task } from 'src/app/core/models/task/task';
import { UserName } from 'src/app/core/models/user/user-name';
import { TasksService } from 'src/app/core/services/tasks.service';
import { SnackBarService } from 'src/app/core/services/shack-bar.service';
import { UsersService } from 'src/app/core/services/users.service';
import { NewTask } from 'src/app/core/models/task/new-task';
import { UnsavedChangesComponent } from 'src/app/core/models/components/unsaved-changes-component';
import { ProjectName } from 'src/app/core/models/project/project-name';
import { ProjectsService } from 'src/app/core/services/projects.service';
import { TaskState } from 'src/app/core/models/task/task-state';

@Component({
  templateUrl: './new-task.component.html',
  styleUrls: ['./new-task.component.scss']
})
export class NewTaskComponent implements OnInit, OnDestroy, UnsavedChangesComponent {
    public task!: Task;
    public projectsOptions: ProjectName[] = [];
    public usersOptions: UserName[] = [];
    public statesEnum = TaskState;

    taskForm!: FormGroup;

    public loading = false;
    public saved = false;

    private unsubscribe$ = new Subject<void>();

    constructor(
        private location: Location,
        private tasksService: TasksService,
        private usersService: UsersService,
        private projectsService: ProjectsService,
        private snackBarService: SnackBarService) {}

    ngOnInit(): void {

        this.taskForm = new FormGroup({
            name: new FormControl('', [Validators.required]),
            description: new FormControl(''),
            finishedAt: new FormControl(''),
            projectId: new FormControl('', [Validators.required]),
            performerId: new FormControl('', [Validators.required]),
            state: new FormControl('', [Validators.required]),
        });

        combineLatest([this.usersService.getUsersNames(), this.projectsService.getProjectsNames()])
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe({
                next: ([usersOptionsResp, projectsOptionsResp]) => {
                    this.usersOptions = usersOptionsResp.body ?? [];
                    this.projectsOptions = projectsOptionsResp.body ?? [];
                },
                error: (error) => {
                    this.snackBarService.showErrorMessage(error?.message || "An error occurred while loading")
                    this.loading = false;
                }
            });
    }

    public saveTask() {
        let newTask = this.taskForm.value as NewTask;

        this.loading = true;
        this.tasksService.createTask(newTask)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe({
                next: (task) => {
                    this.loading = false;
                    this.saved = true;
                    this.goBack();
                    this.snackBarService.showUsualMessage('Successfully created task with ID=' + task?.body?.id);
                },
                error: (error) => {
                    this.snackBarService.showErrorMessage(error?.message || "An error occurred while creating")
                    this.loading = false;
                }
            });
    }

    public goBack = () => this.location.back();

    public hasUnsavedChanges(): boolean {
        return !this.saved && this.taskForm.touched;
    };

    ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
}
