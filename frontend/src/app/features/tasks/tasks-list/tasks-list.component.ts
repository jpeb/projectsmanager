import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subject, takeUntil } from 'rxjs';
import { Task } from 'src/app/core/models/task/task';
import { TasksService } from 'src/app/core/services/tasks.service';
import { SnackBarService } from 'src/app/core/services/shack-bar.service';
import { TaskDeleteDialogComponent } from './task-delete-dialog/task-delete-dialog.component';

@Component({
    templateUrl: './tasks-list.component.html',
    styleUrls: ['./tasks-list.component.scss']
})
export class TasksListComponent implements OnInit, OnDestroy {

    public tasks: Task[] = [];
    public loading = false;

    private unsubscribe$ = new Subject<void>();

    constructor(private dialog: MatDialog, 
        private tasksService: TasksService, 
        private snackBarService: SnackBarService) {}

    ngOnInit(): void {
        this.loadTasks();
    }

    public loadTasks() {
        this.loading = true;
        this.tasksService
            .getTasks()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe({
                next: (resp) => {
                    this.loading = false;
                    this.tasks = resp.body ?? [];
                },
                error: (error) => {
                    this.snackBarService.showErrorMessage(error?.message || "An error occurred while updating")
                    this.loading = false;
                }
            });
    }

    public deleteTask(id: number) {
        this.dialog.open(TaskDeleteDialogComponent, {
            data: { taskId: id },
            autoFocus: false,
            backdropClass: 'dialog-backdrop'
        })
            .afterClosed()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe({
                next: (isDeleted) => {
                    if (isDeleted)
                        this.tasks = this.tasks.filter(e => e.id !== id);
                }
            });
    }

    ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
}
