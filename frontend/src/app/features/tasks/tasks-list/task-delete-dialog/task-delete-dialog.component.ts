import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subject, takeUntil } from 'rxjs';
import { Task } from 'src/app/core/models/task/task';
import { TasksService } from 'src/app/core/services/tasks.service';
import { SnackBarService } from 'src/app/core/services/shack-bar.service';

@Component({
  templateUrl: './task-delete-dialog.component.html',
  styleUrls: ['./task-delete-dialog.component.scss']
})
export class TaskDeleteDialogComponent implements OnInit, OnDestroy {
    public taskId!: number;
    public loading = false;

    private unsubscribe$ = new Subject<void>();

    constructor(
        private dialogRef: MatDialogRef<TaskDeleteDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: { taskId: number },
        private tasksService: TasksService,
        private snackBarService: SnackBarService
    ) {}

    ngOnInit(): void {
        this.taskId = this.data.taskId;
    }

    public deleteTask() {
        this.loading = true;

        this.tasksService
            .deleteTask(this.taskId)
            .pipe(takeUntil(this.unsubscribe$)).subscribe({
                next: () => {
                    this.snackBarService.showUsualMessage('Successfully deleted');
                    this.loading = false;
                    this.dialogRef.close(true);
                },
                error: (error) => {
                    this.snackBarService.showErrorMessage(error?.message || "An error occurred while deleting")
                    this.loading = false;
                    this.dialogRef.close();
                } 
            });
    }

    ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
}
