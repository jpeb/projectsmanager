import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ThemePalette } from '@angular/material/core';
import { Task } from 'src/app/core/models/task/task';

@Component({
    selector: 'app-task-card',
    templateUrl: './task-card.component.html',
    styleUrls: ['./task-card.component.scss']
})
export class TaskCardComponent implements OnInit {
    @Input() public task!: Task;
    @Output() onDelete = new EventEmitter<number>();

    public color: ThemePalette;
    
    constructor() {}

    ngOnInit(): void {
    }

    public deleteTask(){
        this.onDelete.next(this.task.id);
    }
}
