import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { MaterialModule } from 'src/app/shared/material.module';
import { TasksListComponent } from './tasks-list/tasks-list.component';
import { TaskCardComponent } from './tasks-list/task-card/task-card.component';
import { NewTaskComponent } from './new-task/new-task.component';
import { UpdateTaskComponent } from './update-task/update-task.component';
import { ReactiveFormsModule } from '@angular/forms';
import { TaskDeleteDialogComponent } from './tasks-list/task-delete-dialog/task-delete-dialog.component';
import { TaskDetailsComponent } from './task-details/task-details.component';
import { UnsavedChangesWarnGuard } from 'src/app/core/guards/unsaved-changes-warn.guard';
import { PipesModule } from 'src/app/shared/pipes/pipes.module';
import { DirectivesModule } from 'src/app/shared/directives/directives.module';

const routes: Routes = [
    {
        path: '', component: TasksListComponent
    },
    {
        path: 'new', component: NewTaskComponent, canDeactivate: [UnsavedChangesWarnGuard]
    },
    {
        path: ':id', component: TaskDetailsComponent
    },
    {
        path: ':id/edit', component: UpdateTaskComponent, canDeactivate: [UnsavedChangesWarnGuard]
    }
];

@NgModule({
    declarations: [
        TasksListComponent,
        TaskCardComponent,
        NewTaskComponent,
        UpdateTaskComponent,
        TaskDeleteDialogComponent,
        TaskDetailsComponent
    ],
    imports: [
        CommonModule,
        MaterialModule,
        RouterModule.forChild(routes),
        ReactiveFormsModule,
        PipesModule,
        DirectivesModule
    ]
})
export class TasksModule {}
