import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { combineLatest, Subject, takeUntil } from 'rxjs';
import { Task } from 'src/app/core/models/task/task';
import { UpdateTask } from 'src/app/core/models/task/update-task';
import { UserName } from 'src/app/core/models/user/user-name';
import { TasksService } from 'src/app/core/services/tasks.service';
import { SnackBarService } from 'src/app/core/services/shack-bar.service';
import { TeamsService } from 'src/app/core/services/teams.service';
import { UsersService } from 'src/app/core/services/users.service';
import { UnsavedChangesComponent } from 'src/app/core/models/components/unsaved-changes-component';
import { ProjectName } from 'src/app/core/models/project/project-name';
import { TaskState } from 'src/app/core/models/task/task-state';
import { ProjectsService } from 'src/app/core/services/projects.service';

@Component({
    templateUrl: './update-task.component.html',
    styleUrls: ['./update-task.component.scss']
})
export class UpdateTaskComponent implements OnInit, OnDestroy, UnsavedChangesComponent {
    public task!: Task;
    public projectsOptions: ProjectName[] = [];
    public usersOptions: UserName[] = [];
    public statesEnum = TaskState;

    taskForm!: FormGroup;

    public loading = false;
    public saved = false;

    private unsubscribe$ = new Subject<void>();

    constructor(
        private location: Location,
        private route: ActivatedRoute,
        private tasksService: TasksService,
        private usersService: UsersService,
        private projectsService: ProjectsService,
        private snackBarService: SnackBarService) {}

    ngOnInit(): void {

        this.taskForm = new FormGroup({
            name: new FormControl('', [Validators.required]),
            description: new FormControl(''),
            finishedAt: new FormControl(''),
            projectId: new FormControl('', [Validators.required]),
            performerId: new FormControl('', [Validators.required]),
            state: new FormControl('', [Validators.required]),
        });

        this.route.params
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(params => {
                let id = params['id'] as number;
                this.loadTask(id);
            });

            combineLatest([this.usersService.getUsersNames(), this.projectsService.getProjectsNames()])
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe({
                next: ([usersOptionsResp, projectsOptionsResp]) => {
                    this.usersOptions = usersOptionsResp.body ?? [];
                    this.projectsOptions = projectsOptionsResp.body ?? [];
                },
                error: (error) => {
                    this.snackBarService.showErrorMessage(error?.message || "An error occurred while loading")
                    this.loading = false;
                }
            });
    }

    public loadTask(id: number) {
        this.loading = true;
        this.tasksService
            .getTask(id)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe({
                next: (resp) => {
                    this.loading = false;
                    this.task = resp;
                    this.taskForm.patchValue(this.task);
                },
                error: (error) => {
                    this.loading = false;
                    this.goBack();
                    this.snackBarService.showErrorMessage(error?.message || "An error occurred while loading")
                }
            });
    }

    public saveTask() {
        let newTask = this.taskForm.value as UpdateTask;

        if (!this.task?.id) return;

        this.loading = true;
        this.tasksService.updateTask(this.task.id, newTask)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe({
                next: () => {
                    this.loading = false;
                    this.saved = true;
                    this.goBack();
                    this.snackBarService.showUsualMessage('Successfully updated');
                },
                error: (error) => {
                    this.snackBarService.showErrorMessage(error?.message || "An error occurred while updating")
                    this.loading = false;
                }
            });
    }

    public goBack = () => this.location.back();

    public hasUnsavedChanges(): boolean {
        return !this.saved && this.taskForm.touched;
    };

    ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
}
