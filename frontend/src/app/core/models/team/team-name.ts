export interface TeamName {
    id: number;
    name: string;
}
