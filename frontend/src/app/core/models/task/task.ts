import { ProjectName } from "../project/project-name";
import { UserName } from "../user/user-name";
import { TaskState } from "./task-state";

export interface Task {
    id: number;
    createdAt: Date;
    projectId: number;
    project: ProjectName;
    performerId: number;
    performer: UserName;
    name: string;
    description: string;
    state: TaskState;
    finishedAt: Date | null;
}
