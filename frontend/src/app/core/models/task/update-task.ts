import { TaskState } from "./task-state";

export interface UpdateTask {
    projectId: number;
    performerId: number;
    name: string;
    description: string;
    state: TaskState;
    finishedAt: Date | null;
}
