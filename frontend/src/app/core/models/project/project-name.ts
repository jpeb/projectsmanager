export interface ProjectName {
    id: number;
    name: string;
}
