import { Project } from "./project";

export interface ProjectTasksNumber {
    project: Project;
    tasksNumber: number;
}
