import { TeamName } from "../team/team-name";
import { UserName } from "../user/user-name";

export interface Project {
    id: number;
    createdAt: Date;
    authorId: number;
    author: UserName;
    teamId: number;
    team: TeamName;
    name: string;
    description: string;
    deadline: Date;
}
