export interface UpdateProject {
    authorId: number;
    teamId: number;
    name: string;
    description: string;
    deadline: Date;
}
