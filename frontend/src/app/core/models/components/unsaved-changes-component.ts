export interface UnsavedChangesComponent {
    hasUnsavedChanges: () => boolean;
}
