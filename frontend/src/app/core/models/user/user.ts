import { TeamName } from "../team/team-name";

export interface User {
    id: number;
    teamId: number | null;
    team: TeamName | null;
    firstName: string;
    lastName: string;
    email: string;
    registeredAt: Date;
    birthDay: Date;
}
