export interface UpdateUser {
    teamId: number | null;
    firstName: string;
    lastName: string;
    email: string;
    birthDay: Date;
}
