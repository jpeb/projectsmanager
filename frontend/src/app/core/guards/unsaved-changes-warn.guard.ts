import { Injectable, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { map, Observable, Subject, takeUntil } from 'rxjs';
import { UnsavedChangesDialogComponent } from '../components/unsaved-changes-dialog/unsaved-changes-dialog.component';
import { UnsavedChangesComponent } from '../models/components/unsaved-changes-component';

@Injectable({
    providedIn: 'root'
})
export class UnsavedChangesWarnGuard implements CanDeactivate<UnsavedChangesComponent>{

    constructor(private dialog: MatDialog) {}

    canDeactivate(
        component: UnsavedChangesComponent,
        currentRoute: ActivatedRouteSnapshot,
        currentState: RouterStateSnapshot,
        nextState?: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

        return component.hasUnsavedChanges()
            ? this.dialog.open(UnsavedChangesDialogComponent, {
                autoFocus: false,
                backdropClass: 'dialog-backdrop'
            }).afterClosed().pipe(map((e: boolean) => e))
            : true;
    }
}
