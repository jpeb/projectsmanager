import { Injectable } from '@angular/core';
import { NewTask } from '../models/task/new-task';
import { Task } from '../models/task/task';
import { UpdateTask } from '../models/task/update-task';
import { HttpInternalService } from './http-internal.service';

@Injectable({
    providedIn: 'root'
})
export class TasksService {
    public routePrefix = '/api/tasks';

    constructor(private httpService: HttpInternalService) {}

    public getTasks() {
        return this.httpService.getFullRequest<Task[]>(`${this.routePrefix}`);
    }

    public getTask(id: number) {
        return this.httpService.getRequest<Task>(`${this.routePrefix}/${id}`);
    }

    public updateTask(id: number, task: UpdateTask) {
        if(!task.finishedAt) task.finishedAt = null;
        return this.httpService.putFullRequest<Comment>(`${this.routePrefix}/${id}`, task);
    }

    public deleteTask(id: number){
        return this.httpService.deleteFullRequest(`${this.routePrefix}/${id}`);
    }

    public createTask(newTask: NewTask){
        if(!newTask.finishedAt) newTask.finishedAt = null;
        return this.httpService.postFullRequest<Task>(`${this.routePrefix}`, newTask);
    }
}
