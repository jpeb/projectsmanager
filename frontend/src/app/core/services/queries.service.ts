import { Injectable } from '@angular/core';
import { ProjectTasksNumber } from '../models/project/project-tasks-number';
import { HttpInternalService } from './http-internal.service';

@Injectable({
  providedIn: 'root'
})
export class QueriesService {

    constructor(private httpService: HttpInternalService) {}

    public getUserProjectTasksNumber(userId: number) {
        return this.httpService.getRequest<ProjectTasksNumber[]>(`/api/projects/tasksNumber/${userId}`);
    }
}
