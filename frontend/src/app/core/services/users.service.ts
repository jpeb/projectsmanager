import { Injectable } from '@angular/core';
import { NewUser } from '../models/user/new-user';
import { UpdateUser } from '../models/user/update-user';
import { User } from '../models/user/user';
import { UserName } from '../models/user/user-name';
import { HttpInternalService } from './http-internal.service';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
    public routePrefix = '/api/users';

    constructor(private httpService: HttpInternalService) {}

    public getUsers() {
        return this.httpService.getFullRequest<User[]>(`${this.routePrefix}`);
    }

    public getUsersNames() {
        return this.httpService.getFullRequest<UserName[]>(`${this.routePrefix}/names`);
    }

    public getUser(id: number) {
        return this.httpService.getRequest<User>(`${this.routePrefix}/${id}`);
    }

    public updateUser(id: number, user: UpdateUser) {
        return this.httpService.putFullRequest(`${this.routePrefix}/${id}`, user);
    }

    public deleteUser(id: number){
        return this.httpService.deleteFullRequest(`${this.routePrefix}/${id}`);
    }

    public createUser(newUser: NewUser){
        return this.httpService.postFullRequest<User>(`${this.routePrefix}`, newUser);
    }
}
