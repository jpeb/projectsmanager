import { Injectable } from '@angular/core';
import { NewProject } from '../models/project/new-project';
import { Project } from '../models/project/project';
import { ProjectName } from '../models/project/project-name';
import { UpdateProject } from '../models/project/update-project';
import { HttpInternalService } from './http-internal.service';

@Injectable({
    providedIn: 'root'
})
export class ProjectsService {
    public routePrefix = '/api/projects';

    constructor(private httpService: HttpInternalService) {}

    public getProjects() {
        return this.httpService.getFullRequest<Project[]>(`${this.routePrefix}`);
    }

    public getProjectsNames() {
        return this.httpService.getFullRequest<ProjectName[]>(`${this.routePrefix}/names`);
    }

    public getProject(id: number) {
        return this.httpService.getRequest<Project>(`${this.routePrefix}/${id}`);
    }

    public updateProject(id: number, project: UpdateProject) {
        return this.httpService.putFullRequest(`${this.routePrefix}/${id}`, project);
    }

    public deleteProject(id: number){
        return this.httpService.deleteFullRequest(`${this.routePrefix}/${id}`);
    }

    public createProject(newProject: NewProject){
        return this.httpService.postFullRequest<Project>(`${this.routePrefix}`, newProject);
    }
}
