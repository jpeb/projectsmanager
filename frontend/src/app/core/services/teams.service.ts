import { Injectable } from '@angular/core';
import { NewTeam } from '../models/team/new-team';
import { Team } from '../models/team/team';
import { TeamName } from '../models/team/team-name';
import { UpdateTeam } from '../models/team/update-team';
import { HttpInternalService } from './http-internal.service';

@Injectable({
  providedIn: 'root'
})
export class TeamsService {
    public routePrefix = '/api/teams';

    constructor(private httpService: HttpInternalService) {}

    public getTeams() {
        return this.httpService.getFullRequest<Team[]>(`${this.routePrefix}`);
    }

    public getTeamsNames() {
        return this.httpService.getFullRequest<TeamName[]>(`${this.routePrefix}/names`);
    }

    public getTeam(id: number) {
        return this.httpService.getRequest<Team>(`${this.routePrefix}/${id}`);
    }

    public updateTeam(id: number, team: UpdateTeam) {
        return this.httpService.putFullRequest(`${this.routePrefix}/${id}`, team);
    }

    public deleteTeam(id: number){
        return this.httpService.deleteFullRequest(`${this.routePrefix}/${id}`);
    }

    public createTeam(newTeam: NewTeam){
        return this.httpService.postFullRequest<Team>(`${this.routePrefix}`, newTeam);
    }
}
