import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
    templateUrl: './unsaved-changes-dialog.component.html',
    styleUrls: ['./unsaved-changes-dialog.component.scss']
})
export class UnsavedChangesDialogComponent {
    constructor(
        private dialogRef: MatDialogRef<UnsavedChangesDialogComponent>) {}

    public confirmExit() {
        this.dialogRef.close(true)
    }
}
