import { MediaMatcher } from '@angular/cdk/layout';
import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';

@Component({
    selector: 'app-layout',
    templateUrl: './layout.component.html',
    styleUrls: ['./layout.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class LayoutComponent implements OnInit {

    public isShowSidebar: boolean = false;
    public mobileQuery!: MediaQueryList;

    public sidenavItems = [
        {
            title: "Data",
            items: [
                {
                    label: "Projects",
                    matIcon: "fact_check",
                    routerLink: "projects"
                },
                {
                    label: "Tasks",
                    matIcon: "task",
                    routerLink: "tasks"
                },
                {
                    label: "Teams",
                    matIcon: "groups",
                    routerLink: "teams"
                },
                {
                    label: "Users",
                    matIcon: "account_box",
                    routerLink: "users"
                },

            ]
        },
        {
            title: "Queries",
            items: [
                {
                    label: "Project tasks number",
                    matIcon: "query_stats",
                    routerLink: "query/userProjectTasksNumber"
                },
                {
                    label: "Query 2",
                    matIcon: "query_stats",
                    routerLink: "query/query2"
                },
                {
                    label: "Query 3",
                    matIcon: "query_stats",
                    routerLink: "query/query3"
                },
                {
                    label: "Query 4",
                    matIcon: "query_stats",
                    routerLink: "query/query4"
                },
                {
                    label: "Query 5",
                    matIcon: "query_stats",
                    routerLink: "query/query5"
                },
                {
                    label: "Query 6",
                    matIcon: "query_stats",
                    routerLink: "query/query6"
                },
                {
                    label: "Query 7",
                    matIcon: "query_stats",
                    routerLink: "query/query7"
                }

            ]
        }
    ]

    constructor(private changeDetectorRef: ChangeDetectorRef, private media: MediaMatcher) {
    }

    ngOnInit(): void {
        this.mobileQuery = this.media.matchMedia('(max-width: 1024px)');
        this.mobileQuery.addEventListener('change', () => this.changeDetectorRef.detectChanges());

        this.isShowSidebar = !this.mobileQuery.matches;
    }
}
