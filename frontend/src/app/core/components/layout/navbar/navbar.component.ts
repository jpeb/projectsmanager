import { Component, OnInit, ChangeDetectionStrategy, EventEmitter, Output, Input } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NavbarComponent implements OnInit {
    @Input() isMenuOpened = true;
    @Output() isShowSidebar = new EventEmitter<boolean>();
  
    public openMenu(): void {
      this.isMenuOpened = !this.isMenuOpened;
      this.isShowSidebar.emit(this.isMenuOpened);
    }

    ngOnInit() {
    }
  }
  