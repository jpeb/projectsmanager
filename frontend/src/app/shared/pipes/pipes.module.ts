import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LocalDatePipe } from './local-date.pipe';
import { EnumToArrayPipe } from './enum-to-array.pipe';



@NgModule({
    declarations: [
        LocalDatePipe,
        EnumToArrayPipe
    ],
    imports: [
        CommonModule
    ],
    exports: [
        LocalDatePipe,
        EnumToArrayPipe
    ]
})
export class PipesModule {}
