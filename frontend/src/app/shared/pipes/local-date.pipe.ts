import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'localDate'
})
export class LocalDatePipe implements PipeTransform {
    transform(value?: Date, locale: string = 'uk-UA'): string {
        return value ? new Date(value).toLocaleDateString(locale, { year: 'numeric', month: 'long', day: 'numeric' }) : '';
    }
}
