import { Directive, ElementRef, Input, OnChanges, SimpleChanges } from '@angular/core';
import { TaskState } from 'src/app/core/models/task/task-state';

@Directive({
    selector: '[taskStateChip]'
})
export class TaskStateChipDirective implements OnChanges {

    stateColors = [
        { name: 'Created', state: TaskState.Created, color: "mat-default" },
        { name: 'Canceled', state: TaskState.Canceled, color: 'mat-warn' },
        { name: 'Finished', state: TaskState.Finished, color: 'mat-accent' },
        { name: 'In Progress', state: TaskState.InProgress, color: 'mat-primary' },
    ];

    @Input() taskStateChip?: TaskState;

    constructor(private element: ElementRef) {
    }
    
    ngOnChanges(changes: SimpleChanges): void {
        if (this.taskStateChip !== null && this.taskStateChip !== undefined) {
            let stateColor = this.stateColors.find(c => c.state === this.taskStateChip)!;
            this.element.nativeElement.classList.add(stateColor.color);
            this.element.nativeElement.innerHTML = stateColor.name;
        }
    }

}
