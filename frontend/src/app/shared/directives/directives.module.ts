import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TaskStateChipDirective } from './task-state-chip.directive';



@NgModule({
    declarations: [
        TaskStateChipDirective
    ],
    imports: [
        CommonModule
    ],
    exports: [
        TaskStateChipDirective
    ]
})
export class DirectivesModule {}
